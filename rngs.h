/*
 * @(#)rngs.h
 *
 * Dr. Park's algorithm published in the Oct. '88 ACM
 * "Random Number Generators: Good Ones Are Hard To Find"
 * His version available at ftp://cs.wm.edu/pub/rngs.tar
 * Present form by many authors.
 */

#ifndef _rngs_h
#define _rngs_h

/*** random number generator ***/
#if HAVE_RAND48
#define SRAND srand48
#define LRAND lrand48
#else /* HAVE_RAND48 */
#if HAVE_RANDOM
#define SRAND srandom
#define LRAND random
#else /* HAVE_RANDOM */
#if HAVE_RAND
#define SRAND srand
#define LRAND rand
#endif /* HAVE_RAND */
#endif /* HAVE_RANDOM */
#endif /* HAVE_RAND48 */
#ifndef SRAND
extern void SetRNG(long s);
#define SRAND(X) SetRNG((long) X)
#endif
#ifndef LRAND
extern long LongRNG(void);
#define LRAND() LongRNG()
#endif
#define NRAND(X) ((int)(LRAND()%(X)))

#endif /* _rngs_h */
