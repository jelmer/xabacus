/*
 * @(#)xwin.c
 *
 * Copyright 2005 - 2018  David A. Bagley, bagleyd AT verizon.net
 *
 * All rights reserved.
 *
 * Permission to use, copy, modify, and distribute this software and
 * its documentation for any purpose and without fee is hereby granted,
 * provided that the above copyright notice appear in all copies and
 * that both that copyright notice and this permission notice appear in
 * supporting documentation, and that the name of the author not be
 * used in advertising or publicity pertaining to distribution of the
 * software without specific, written prior permission.
 *
 * This program is distributed in the hope that it will be "useful",
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

/* Methods file for xwin */

#include "xwin.h"

#ifdef WINVER
void
FILLPOLYGON(HDC hDC, HPEN *hPen, HPEN *hOldPen,
		HBRUSH *hBrush, HBRUSH *hOldBrush,
		GC color, GC lineColor, const POINT *poly, int n,
		Boolean origin)
{
	/* CoordModePrevious -> CoordModeOrigin */
	POINT *temp = NULL;
	int pt;

	if (!origin) {
		if (!(temp = (POINT *) malloc(sizeof (POINT) * (size_t) n))) {
			DISPLAY_ERROR("Not enough memory (POLYGON), exiting.");
		}
		temp[0] = poly[0];
		for (pt = 1; pt < n; pt++) {
			temp[pt].x = temp[pt - 1].x + poly[pt].x,
			temp[pt].y = temp[pt - 1].y + poly[pt].y;
		}
	}
	*hPen = CreatePen(PS_SOLID, 1, lineColor);
	*hOldPen = (HPEN) SelectObject(hDC, *hPen);
	*hBrush = CreateSolidBrush(color);
	*hOldBrush = (HBRUSH) SelectObject(hDC, *hBrush);
	(void) Polygon(hDC, (origin) ? poly : temp, n);
	(void) SelectObject(hDC, *hOldBrush);
	(void) DeleteObject(*hBrush);
	(void) SelectObject(hDC, *hOldPen);
	(void) DeleteObject(*hPen);
	if (!origin) {
		free(temp);
	}
}

void
DRAWPOLYLINE(HDC hDC, HPEN *hPen, HPEN *hOldPen,
		GC color, const POINT *poly, int n,
		Boolean origin)
{
	/* CoordModePrevious -> CoordModeOrigin */
	POINT *temp = NULL;
	int pt;

	if (!origin) {
		if (!(temp = (POINT *) malloc(sizeof (POINT) * (size_t) n))) {
			DISPLAY_ERROR("Not enough memory (POLYLINE), exiting.");
		}
		temp[0] = poly[0];
		for (pt = 1; pt < n; pt++) {
			temp[pt].x = temp[pt - 1].x + poly[pt].x,
			temp[pt].y = temp[pt - 1].y + poly[pt].y;
		}
	}
	*hPen = CreatePen(PS_SOLID, 1, color);
	*hOldPen = (HPEN) SelectObject(hDC, *hPen);
	(void) Polyline(hDC, (origin) ? poly : temp, n);
	(void) SelectObject(hDC, *hOldPen);
	(void) DeleteObject(*hPen);
	if (!origin) {
		free(temp);
	}
}
#endif

void
intCat(char **string, const char *var1, const int var2)
{
	unsigned int stringSize = strlen(var1) + 21;
	if (!(*string = (char *) malloc(stringSize))) {
		DISPLAY_ERROR("Not enough memory (intCat), exiting.");
	}
#ifdef HAVE_SNPRINTF
	(void) snprintf(*string, stringSize, "%s%d", var1, var2);
#else
	(void) sprintf(*string, "%s%d", var1, var2);
#endif
}

void
stringCat(char **string, const char *var1, const char *var2)
{
	unsigned int stringSize = strlen(var1) + strlen(var2) + 1;
	if (!(*string = (char *) malloc(stringSize))) {
		DISPLAY_ERROR("Not enough memory (stringCat), exiting.");
	}
#ifdef HAVE_SNPRINTF
	(void) snprintf(*string, stringSize, "%s%s", var1, var2);
#else
	(void) sprintf(*string, "%s%s", var1, var2);
#endif
}

void
intCpy(char *string, const int var)
{
#ifdef HAVE_SNPRINTF
	(void) snprintf(string, 21, "%d", var);
#else
	(void) sprintf(string, "%d", var);
#endif
}

