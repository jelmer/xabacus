/*
 * @(#)timer.h
 *
 * Copyright 2005 - 2017  David A. Bagley, bagleyd AT verizon.net
 *
 * All rights reserved.
 *
 * Permission to use, copy, modify, and distribute this software and
 * its documentation for any purpose and without fee is hereby granted,
 * provided that the above copyright notice appear in all copies and
 * that both that copyright notice and this permission notice appear in
 * supporting documentation, and that the name of the author not be
 * used in advertising or publicity pertaining to distribution of the
 * software without specific, written prior permission.
 *
 * This program is distributed in the hope that it will be "useful",
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

/* Header file for timer */

#ifndef _timer_h
#define _timer_h

#ifdef WINVER
#include <windows.h>
#include <time.h>

#if ((WINVER > 0x030a) && !defined(GCL_HBRBACKGROUND) && !defined(GCLP_HBRBACKGROUND))
#undef WINVER
#define WINVER 0x030a
#endif
#if (WINVER <= 0x030a)		/* if WINDOWS 3.1 or less */
extern void Sleep(unsigned long cMilliseconds);
#endif
#define TimeVal long

#define initTimer(v) v = (long) GetTickCount()

#else
#include <X11/Xos.h>

#ifndef TimeVal
#define TimeVal struct timeval
#endif

#define initTimer(v) (void) X_GETTIMEOFDAY(&(v));

extern void Sleep(unsigned int cMilliseconds);
#endif

extern void useTimer(TimeVal * oldTime, int delay);

#endif /* _timer_h */
