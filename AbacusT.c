/*
 * @(#)AbacusT.c
 *
 * Copyright 2009 - 2018  David A. Bagley, bagleyd AT verizon.net
 *
 * All rights reserved.
 *
 * Permission to use, copy, modify, and distribute this software and
 * its documentation for any purpose and without fee is hereby granted,
 * provided that the above copyright notice appear in all copies and
 * that both that copyright notice and this permission notice appear in
 * supporting documentation, and that the name of the author not be
 * used in advertising or publicity pertaining to distribution of the
 * software without specific, written prior permission.
 *
 * This program is distributed in the hope that it will be "useful",
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Used tables from How to Learn Lee's Abacus by Lee Kai-chen.
 */

#include "AbacusP.h"

#ifdef HAVE_GETTEXT
#include <stdio.h>
#include <stdlib.h>
#include <libintl.h>
#include <locale.h>
#define _(STRING) gettext(STRING)
#else
#define _(STRING) STRING
#endif

#define CHAR_TO_DIGIT(c) ((c >= 'A') ? c - 'A' + 10 : c - '0')
#define DIGIT_TO_CHAR(d) ((d >= 10) ? (char) ('A' + d - 10) : (char) ('0' + d))
#define IS_DIGIT(c) ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'J'))
#define EPSILON 0.0000001

/* The KOJIMA stuff is not totally fleshed out.
 * Also when enabled will also show some of the math algorithms
 * used for understanding LEE which might not work fully.
 */
#ifdef KOJIMA_ROOT
#undef LEE_ROOT
#else
#define LEE_ROOT
#endif

#ifdef TEST
static void testTeachRoot(AbacusWidget w,
	int startX, int finishX, int max);
#else
static Boolean hOn = FALSE;
static int hAux = 0;
static int hRail = 0;
#endif

/* This ignores strange characters */
static void simpleParser(AbacusWidget w, char * buffer,
		char* aString, char* bString, char *op)
{
	unsigned int i;
	int numberCount = 0, decimalCount = 0;
	Boolean digit = False, decimal = False, negate = False;
	int decimalPosition = w->abacus.decimalPosition;

	if (w->abacus.decks[BOTTOM].piece != 0)
		decimalPosition--;
	if (w->abacus.decimalPosition >= w->abacus.shiftPercent &&
			w->abacus.decks[BOTTOM].piecePercent != 0)
		decimalPosition--;
	aString[0] = '\0';
	bString[0] = '\0';
	*op = ' ';
	for (i = 0; i < strlen(buffer); i++) {
		if (buffer[i] == 'q' || buffer[i] == 'Q') {
			*op = buffer[i];
			return;
		}
		if ((buffer[i] == '+' || buffer[i] == '-') &&
				(numberCount == 0 || *op != ' ') && !digit) {
			if (buffer[i] == '-')
				negate = !negate;
		} else if (buffer[i] == '+' || buffer[i] == '-' ||
				buffer[i] == '*' || buffer[i] == '/' ||
				buffer[i] == 'v' || buffer[i] == 'u') {
			if (*op != ' ')
				return;
			*op = buffer[i];
			numberCount++;
			decimalCount = 0;
			digit = False;
			decimal = False;
			negate = False;
		} else if (buffer[i] == '.' && !decimal) {
			decimal = True;
			if (numberCount == 0)
				(void) strncat(aString, ".", 2);
			else
				(void) strncat(bString, ".", 2);
		} else if (IS_DIGIT(buffer[i])) {
			if (!decimal || decimalCount < decimalPosition) {
				if (numberCount == 0) {
					if (!digit && negate)
						(void) strncat(aString, "-", 2);
					aString = strAddChar(aString, buffer[i]);
				} else {
					if (!digit && negate)
						(void) strncat(bString, "-", 2);
					bString = strAddChar(bString, buffer[i]);
				}
				negate = False;
			}
			if (decimal) {
				decimalCount++;
			}
			digit = True;
		}
	}
}

/* More generic */
#if 1
/* Taken from addition tables in "How to Learn Lee's Abacus", */
/* made a little more generic, to handle other bases. */
static void digitAdd(int a, int b, int carryIn,
		int base, int topFactor, int bottomNumber,
		int* lower, int* upper, int* carryOut)
{
	int newB = b + carryIn;
	int fractBase;
	int modB, modAns, ans;
	int divA, modDivAns, divAns;

	if (bottomNumber > base / 2) /* topFactor may not be set, Russian */
		fractBase = base;
	else
		fractBase = topFactor;
	modB = newB % fractBase;
	ans = a + newB;
	modAns = ans % fractBase;
	divA = a / fractBase;
	modDivAns = (ans % base) / fractBase;
	divAns = ans / base;
	if (modAns - modB >= 0)
		*lower = modB;
	else
		*lower = modB - fractBase;
	*upper = modDivAns - divA;
	*carryOut = divAns;
#ifdef OLD_DEBUG
	(void) printf("add:\t%d\t%d\t%d\t|\t%d\t%d\t%d\n", a, b, carryIn,
		*lower, *upper, *carryOut);
#endif
}

/* Taken from subtraction tables in "How to Learn Lee's Abacus", */
/* made a little more generic, to handle other bases. */
static void digitSubtract(int a, int b, int carryIn,
		int base, int topFactor, int bottomNumber,
		int* lower, int* upper, int* carryOut)
{
	int newB = b - carryIn;
	int fractBase;
	int modA, modB, ans;
	int divA, modDivAns, divAns;

	if (bottomNumber > base / 2) /* topFactor may not be set, Russian */
		fractBase = base;
	else
		fractBase = topFactor;
	modA = a % fractBase;
	modB = newB % fractBase;
	ans = a - newB;
	divA = a / fractBase;
	modDivAns = ((ans + base) % base) / fractBase;
	divAns = (ans - base + 1) / base;
	if (modA - modB >= 0)
		*lower = -modB;
	else
		*lower = fractBase - modB;
	*upper = modDivAns - divA;
	*carryOut = divAns;
#ifdef OLD_DEBUG
	(void) printf("sub:\t%d\t%d\t%d\t|\t%d\t%d\t%d\n", a, b, carryIn,
		*lower, *upper, *carryOut);
#endif
}

#else
/* Taken from addition tables in "How to Learn Lee's Abacus" */
/* less generic, but easier to follow from book. */
static void digitAdd(int a, int b, int carryIn,
		int base, int topFactor, int bottomNumber,
		int* lower, int* upper, int* carryOut)
{
	int newB = b + carryIn;
	int fractBase;

	if (bottomNumber > base / 2) /* topFactor may not be set */
		fractBase = base;
	else
		fractBase = topFactor;
	*lower = 0;
	*upper = 0;
	*carryOut = 0;
	if (newB < fractBase) {
		if (a < fractBase) {
			if ((a + newB) < fractBase) { /* table 1 */
				*lower = newB;
			} else { /* table 2 */
				*lower = newB - fractBase;
				if (bottomNumber > base / 2)
					*carryOut = 1;
				else
					*upper = 1;
			}
		} else {
			if ((a + newB) < base) { /* table 1 */
				*lower = newB;
			} else { /* table 3 */
				*lower = newB - fractBase;
				*upper = -1;
				*carryOut = 1;
			}
		}
	} else {
		if ((a + newB) < base) { /* table 1 */
			*lower = newB - fractBase;
			*upper = 1;
		} else if (a >= fractBase && a + newB < base + fractBase) { /* table 4 */
			*lower = newB - fractBase;
			*upper = -1;
			*carryOut = 1;
		} else { /* table 3 */
			*lower = newB - base;
			*carryOut = 1;
		}
	}
#ifdef OLD_DEBUG
	(void) printf("add %d\t%d\t%d\t|\t%d\t%d\t%d\n", a, b, carryIn,
		*lower, *upper, *carryOut);
#endif
}

/* Taken from subtraction tables in "How to Learn Lee's Abacus" */
/* less generic but easier to follow from book. */
static void digitSubtract(int a, int b, int carryIn,
		int base, int topFactor, int bottomNumber,
		int* lower, int* upper, int* carryOut)
{
	int newB = b - carryIn;
	int fractBase; /* Kludge but may not be a bad fix here */
	if (bottomNumber > base / 2) /* topFactor may not be set */
		fractBase = base;
	else
		fractBase = topFactor;
	*lower = 0;
	*upper = 0;
	*carryOut = 0; /* if used is negative, i.e. a borrow */
	if (newB <= fractBase) {
		if (a < fractBase) {
			if ((a - newB) >= 0) { /* table 1 */
				*lower = -newB;
			} else { /* table 3 */
				*lower = fractBase - newB;
				if (bottomNumber <= base / 2)
					*upper = 1;
				*carryOut = -1;
			}
		} else {
			if ((a - newB) >= fractBase) { /* table 2, 3 */
				*lower = -newB;
			} else { /* table 1 */
				*lower = fractBase - newB;
				*upper = -1;
			}
		}
	} else {
		if ((a - newB) >= 0) { /* table 1 */
			*lower = fractBase - newB;
			*upper = -1;
		} else if (a < fractBase && (a - newB) >= -fractBase) { /* table 4 */
			*lower = fractBase - newB;
			*upper = 1;
			*carryOut = -1;
		} else { /* table 3 */
			*lower = base - newB;
			*carryOut = -1;
		}
	}
#ifdef OLD_DEBUG
	(void) printf("sub %d\t%d\t%d\t|\t%d\t%d\t%d\n", a, b, carryIn,
		*lower, *upper, *carryOut);
#endif
}
#endif

/*
Guide:
2+1, 2+6, 2+4, 9+7, 36+75
10-3, 12-6, 100-58
67*2, 9*7, 26*14, 678*345
93/3, 1476/12, 638/22

Lee:
859*7, 54*23, 864*315, 26.5*3.4, .753*.04
4571/7, 7448/76, 72.61/2.74
625v, 1664.64v, 1004004v
12167u, 8615.125u, 8036.054027u
*/

#ifdef OLD_DEBUG
/* Unit Test */
static void testTable()
{
	int i, j, carryIn, k, lower, upper, carryOut;
#if 1
	int base = DEFAULT_BASE;
	int factor = 5;
#else
	int base = 16;
	int factor = 4;
#endif
	int c = 1;
	/*int c = 2;*/

	for (k = 0 ; k < c; k++) {
		for (j = 0 ; j < base; j++) {
			for (i = 0 ; i < base; i++) {
				carryIn = k;
#if 0
				digitAdd(i, j, carryIn,
					base, factor, factor,
					&lower, &upper, &carryOut);
#else
				digitSubtract(i, j, carryIn,
					base, factor, factor,
					&lower, &upper, &carryOut);
#endif
				(void) printf("%d\t%d\t%d\t|\t%d\t%d\t%d\n",
					i, j, carryIn, lower, upper, carryOut);
			}
		}
	}
}
#endif

/* Feeback lines to user */
#ifdef WINVER
static void drawLineText(AbacusWidget w, const char* line, int i)
{
#ifdef DEMO_FRAMED
	(void) SetTextColor(w->core.hDC, w->abacusDemo.foregroundGC);
	(void) SetBkMode(w->core.hDC, TRANSPARENT);
	(void) TextOut(w->core.hDC,
		1,
		w->core.height + w->abacusDemo.fontHeight * i - 1,
		line, (signed) strlen(line));
#else
	if (w == NULL) {
		return;
	}
	drawTeachText(line, i);
}
#endif
#else
static void drawLineText(AbacusWidget w, char* line, int i)
{
	setAbacusText(w, ACTION_TEACH_LINE, line, i);
}
#endif

/* Position of decimal point in string */
static int getDecimalStringPosition(char * string)
{
	int i = 0;

	while (string[i] != '\0') {
		if (string[i] == '.')
			return i;
		i++;
	}
	return i;
}

/* Position of decimal point in int array */
static int arrayLen(int * array)
{
	int i = 0;

	while (array[i] != -2) {
		i++;
	}
	return i;
}

/* Position of decimal point in int array */
static int getDecimalArrayPosition(int * array)
{
	int i = 0;

	while (array[i] != -2) {
		if (array[i] == -1)
			return i;
		i++;
	}
	return i;
}

/* Appends decimal where necessary for consistency */
static void decimalSafe(char * string)
{
	int decimal = -1;

	decimal = getDecimalStringPosition(string);
	if (decimal == (int) strlen(string)) {
		(void) strncat(string, ".", 2);
	}
}

/* Find decimal place offset given current position */
static int decimalPlaceString(char * string, int pos)
{
	int i = getDecimalStringPosition(string);

	if (i == (int) strlen(string) || i >= pos)
		i -= (pos + 1);
	else
		i -= pos;
#ifdef OLD_DEBUG
	(void) printf("decimalPlaceString return %d: %s, position %d\n",
		i, string, pos);
#endif
	return i;
}

/* Find decimal place offset given current position */
static int decimalPlaceArray(int * array, int pos)
{
	int i = getDecimalArrayPosition(array);

	if (i == arrayLen(array) || i >= pos)
		i -= (pos + 1);
	else
		i -= pos;
	return i;
}

/* Find position offset given current place */
static int decimalOffsetString(char * string, int place)
{
	int i = getDecimalStringPosition(string);

	if (place >= 0)
		i -= (place + 1);
	else
		i -= place;
#ifdef OLD_DEBUG
	(void) printf("decimalOffsetString return %d: %s, place %d\n",
		i, string, place);
#endif
	return i;
}

#ifdef EXTRA
/* Find position offset given current place */
static int decimalOffsetArray(int * array, int place)
{
	int i = getDecimalArrayPosition(array);

	if (place >= 0)
		i -= (place + 1);
	else
		i -= place;
	return i;
}
#endif

/* Contract StringBuffer to remove leading and trailing 0's */
static void contractStringBuffer(char * string)
{
	int offset = getDecimalStringPosition(string);
	int i, length = (int) strlen(string);

	for (i = 0; i < offset - 1; i++) {
		if (string[0] == '0')
			(void) strncpy(string, &(string[1]), (unsigned int) length);
		else
			break;
	}
	offset = getDecimalStringPosition(string);
	length = (int) strlen(string);
	if (offset < length)
		for (i = length - 1; i > 1; i--) {
			if (string[i] == '0')
				string[i] = '\0';
			else
				break;
		}
	length = (int) strlen(string);
	if (string[0] != '\0' && string[0] == '.') { /* normalize */
		for (i = length; i >= 0; i--) {
			string[i + 1] = string[i];
		}
		string[0] = '0';
	}
}

#ifdef LEE_ROOT
static void trimStringBuffer(char *string)
{
	int last;

	contractStringBuffer(string);
	last = (int) strlen(string) - 1;
	if (string[last] == '.')
		string[last] = '\0';
}

static void stripDecimal(char * string) {
	int i, found = 0, length;

	for (i = 0; i < (int) strlen(string); i++) {
		if (found == 1 || string[i] == '.') {
			string[i] = string[i + 1];
			found = 1;
		}
	}
	found = -1;
	for (i = 0; i < (int) strlen(string); i++)
		if (string[i] != '0') {
			found = i;
			break;
		}
	if (found == -1) {
		string[0] = '0';
		string[1] = '\0';
		return;
	}
	if (found == 0)
		return;
	length = (int) strlen(string);
	for (i = found; i < length; i++)
		string[i - found] = string[i];
	string[length - found] = '\0';
}

/* Strip StringBuffer of trailing len numbers */
static void stripStringBuffer(char *string, int len, int steps)
{
	int length, i;

	trimStringBuffer(string);
	stripDecimal(string);
	length = (int) strlen(string);
	if (len >= 0) {
		if (length >= len)
			string[length - len] = '\0';
	} else {
		/* if negative have to add back */
		if (length < steps) {
			for (i = length; i < steps; i++) {
				string[i] = '0';
			}
			string[i] = '\0';
		}
	}
#ifdef OLD_DEBUG
	(void) printf("stripStringBuffer: %s %d\n", string, (int) strlen(string));
#endif
}
#endif

/* Expand StringBuffer to fit decimal place */
static void expandStringBuffer(char * string, int place)
{
	int offset = getDecimalStringPosition(string);
	int prependOffset = place - offset + 1;
	int appendOffset = offset + 1 - (int) strlen(string) - place;
	int i, len;

	decimalSafe(string);
	len = (int) strlen(string);
	if (place > 0 && prependOffset > 0) {
		for (i = len; i >= 0; i--) {
			string[i + prependOffset] = string[i];
		}
		for (i = 0; i < prependOffset; i++) {
			string[i] = '0';
		}
	} else if (place < 0 && appendOffset > 0) {
		for (i = 0; i < appendOffset; i++) {
			string[len + i] = '0';
		}
		string[len + appendOffset] = '\0';
	}
}

/* Shift StringBuffer to fit decimal place */
static void shiftStringBuffer(char * string, int shift)
{
	int offset = getDecimalStringPosition(string);
	int newOffset, i, j, k;

	j = (int) strlen(string);
	if (offset != j)
		for (i = 0; i < j - offset; i++) {
			string[offset + i] = string[offset + i + 1];
		}
	j = (int) strlen(string);
	if (offset >= j) {
		while (j > 1 && string[j - 1] == '0') {
			string[j - 1] = string[j];
		}
	}
	if (offset == 1 && string[0] == '0') {
		while (((int) strlen(string)) > 1 &&
				string[0] == '0') {
			j = (int) strlen(string);
			for (i = 1; i <= j; i++) {
				string[i - 1] = string[i];
			}
			offset--;
		}
	}
	if (string[0] == '0' && ((int) strlen(string)) == 1) {
		return;
	}
	newOffset = offset + shift;
	if (newOffset <= 0) {
		for (i = 0; i < -newOffset; i++) {
			j = (int) strlen(string);
			for (k = j; k >= 0; k--) {
				string[k + 1] = string[k];
			}
			string[0] = '0';
		}
		j = (int) strlen(string);
		for (i = j; i >= 0; i--) {
			string[i + 2] = string[i];
		}
		string[0] = '0';
		string[1] = '.';
	} else if (newOffset >= (int) strlen(string)) {
		newOffset -= (int) strlen(string);
		for (i = 0; i < newOffset; i++) {
			j = (int) strlen(string);
			string[j] = '0';
			string[j + 1] = '\0';
		}
	} else {
		for (i = (int) strlen(string); i >= newOffset; i--) {
			string[i + 1] = string[i];
		}
		string[newOffset] = '.';
	}
}

#ifdef OLD_DEBUG
void testShift(char *string)
{
	char my[512], yours[512];
	int i;

	(void) strcpy(my, string);
	for (i = -4; i < 5; i++) {
		(void) strncpy(yours, my, 512);
		shiftStringBuffer(yours, i);
		(void) printf("my %s, i %d, yours %s\n", my, i, yours);
	}
}
#endif

static void
stringGroup(int *intGroup, char *group, char v)
{
	char pre[2];
	int intGroupSize = sizeof(intGroup) * 11;
	char *temp = (char *) malloc(sizeof (char) * (unsigned int) intGroupSize);

	pre[0] = '\0';
	pre[1] = '\0';
	temp[0] = '\0';
	for (; *intGroup != -2; intGroup++) {
		(void) strncpy(temp, group, (unsigned int) intGroupSize);
		if (*intGroup == -1) {
			/* 1 byte */
			(void) sprintf(group, "%s%s%s",
				temp, pre, ".");
		} else {
			if (v == 'v')
				/* 2-11 bytes */
				(void) sprintf(group, "%s%s%02d",
					temp, pre, *intGroup);
			else
				/* 3-11 bytes */
				(void) sprintf(group, "%s%s%03d",
					temp, pre, *intGroup);
		}
		pre[0] = ' ';
	}
	free(temp);
}

/* Set string given change in rail (decimal position) */
static void setStringBuffer(AbacusWidget w, char * string, int aux,
		int place, int lower, int upper)
{
	int offset, digit;
	int topFactor, bottomNumber;
	AbacusWidget *wap = &w, wa;

	if (aux == 1 && w->abacus.leftAux != NULL)
		wap = (AbacusWidget *) w->abacus.leftAux;
	else if (aux == 2 && w->abacus.rightAux != NULL)
		wap = (AbacusWidget *) w->abacus.rightAux;
	wa = *wap;
	topFactor = wa->abacus.decks[TOP].factor;
	bottomNumber = wa->abacus.decks[BOTTOM].number;
	if (bottomNumber <= wa->abacus.base / 2)
		digit = lower + upper * topFactor;
	else
		digit = lower;
	expandStringBuffer(string, place);
	offset = decimalOffsetString(string, place);
	string[offset] = DIGIT_TO_CHAR((digit + wa->abacus.base +
		CHAR_TO_DIGIT(string[offset])) % wa->abacus.base);
#ifdef OLD_DEBUG
	(void) printf("setStringBuffer %s %d\n", string, place);
#endif
}

/* Appends 0's where necessary to make adding is easier */
/* (before and after decimal point) */
static void addSafe(char * aString, char * bString)
{
	int aDecimal = -1, bDecimal = -1;
	int aCount, bCount, i;
	char temp[512];

#ifdef OLD_DEBUG
	(void) printf("addSafe start %s %s\n", aString, bString);
#endif
	aDecimal = getDecimalStringPosition(aString);
	bDecimal = getDecimalStringPosition(bString);
	aCount = (int) strlen(aString) - aDecimal;
	bCount = (int) strlen(bString) - bDecimal;

	if (aCount > bCount) {
		for (i = bCount; i < aCount; i++)
			(void) strncat(bString, "0", 2);
	} else {
		for (i = aCount; i < bCount; i++)
			(void) strncat(aString, "0", 2);
	}
	if (aDecimal > bDecimal) {
		for (i = bDecimal; i < aDecimal; i++) {
			(void) strcpy(temp, bString);
			(void) sprintf(bString, "%c%s", '0', temp);
		}
	} else {
		for (i = aDecimal; i < bDecimal; i++) {
			(void) strcpy(temp, aString);
			(void) sprintf(aString, "%c%s", '0', temp);
		}
	}
#ifdef OLD_DEBUG
	(void) printf("addSafe final %s %s\n", aString, bString);
#endif
}

/* Next digit in string according to step */
static int nextCharPosition(int digitStep, int digitCarryStep,
		Boolean rightToLeft, char * string)
{
	int n, a;
	int count = (int) strlen(string) - 1;
	int decimalPosition = (int) strlen(string) - 1 -
		getDecimalStringPosition(string);
	int decimal = 0;

	if ((int) strlen(string) == getDecimalStringPosition(string))
		count = (int) strlen(string); /* no decimal point */
	n = digitStep;
	if (rightToLeft) {
		if (digitCarryStep != 0)
			n += (digitCarryStep / 2);
		if (n >= decimalPosition) {
			decimal++;
		}
		n += decimal;
		a = count - n;
	} else {
		if (digitCarryStep != 0)
			n += -(digitCarryStep / 2);
		if (n >= count - decimalPosition) {
			decimal++;
		}
		n += decimal;
		a = n;
	}
#ifdef OLD_DEBUG
	(void) printf("nextCharPosition: %d at step %d in %s, decimalPosition %d, n %d, decimal %d, count %d\n",
		a, digitStep, string, decimalPosition, n, decimal, count);
#endif
	return a;
}

/* Digit at position in string */
static int nextChar(char * string, int pos)
{
	int digit;

	if (pos < 0 || pos >= (int) strlen(string)) {
		digit = 0;
	} else {
		digit = CHAR_TO_DIGIT(string[pos]);
	}
#ifdef OLD_DEBUG
	(void) printf("nextChar: %d at %d in %s\n", digit, pos, string);
#endif
	return digit;
}

#ifdef LEE_ROOT
static int placeGroup(int *intGroup)
{
	int i = 0;

	for (; *intGroup != -2; intGroup++) {
		if (*intGroup == -1) {
			return i;
		}
		i++;
	}
	return i;
}

/* int value of string offset by place */
static int andAbove(char * string, int place, int base)
{
	int i, value = 0, factor = 1, pos;
	int runover = decimalOffsetString(string, place) - (int) strlen(string);
	int newPlace = place;

	if (runover > 0)
		newPlace += runover;
	for (i = 0; i < (int) strlen(string); i++) {
		pos = decimalOffsetString(string, newPlace + i);
		if (pos < 0)
			break;
		value += nextChar(string, pos) * factor;
		factor *= base;
	}
	for (i = 0; i < runover; i++) {
		value *= base;
	}
#ifdef OLD_DEBUG
	(void) printf("andAbove: return %d at place %d in string %s, runover%d\n",
		value, place, string, runover);
#endif
	return value;
}

/* number of places where int value of string offset by place */
static int andAbovePlaces(char * string, int place)
{
	int i, j, pos = 0;
	int runover = decimalOffsetString(string, place) - (int) strlen(string);
	int newPlace = place;
	Boolean decimalValue = False;
	Boolean intValue = False;

	if (runover > 0)
		newPlace += runover;
	for (i = 0; i < (int) strlen(string); i++) {
		pos = decimalOffsetString(string, newPlace + i);
		if (pos < 0) {
			break;
		}
	}
	/* Handle numbers < 1 */
	for (j = 0; j < (int) strlen(string); j++) {
		if (string[j] == '0') {
			if (decimalValue)
				i--;
		} else if (string[j] == '.') {
			i--;
			decimalValue = True;
		} else {
			if (!decimalValue)
				intValue = True;
			break;
		}
	}
	if (!intValue && i <= 0)
		i = 1;
	if (runover > 0)
		i += runover;

#ifdef OLD_DEBUG
	(void) printf("andAbovePlaces: return %d at place %d in string %s, runover%d, pos %d, intValue %d\n",
		i, place, string, runover, pos, intValue);
#endif
	return i;
}

#if 0
static int digitInPlace(int value, int pos, int base)
{
	int i;

	for (i = 0; i < pos; i++)
		value /= base;
	return value % base;
}
#endif

static int logInt(int number, int base)
{
	int value = number;
	int count = 0;

	while (value >= base) {
		value /= base;
		count++;
	}
	return count;
}

#ifdef TEST
static double expFloat(int place, int base)
{
	double exp = 1.0;
	int i;

	for (i = 0; i < place; i++)
		exp /= base;
	return exp;
}
#endif

#if 0
static int logFloat(double fract, int base)
{
	double value = fract;
	int count = 0;

	if (fract <= 0)
		return 0; /* actually an error but do not care */
	if (fract > 1.0)
		while (value >= base) {
			value /= base;
			count++;
		}
	else if (fract < 1.0)
		while (value < 1.0) {
			value *= base;
			count--;
		}
	return count;
}
#endif
#endif

/* Number of steps in addition and subtraction */
static int addSteps(char * string)
{
	/* decimal included */
	return (int) strlen(string) - 1;
}

/* Number of multiply steps in multiplication */
static int multSteps(char * aString, char * bString)
{
	/* decimal included */
	return ((int) strlen(aString) - 1) * ((int) strlen(bString) - 1);
}

/* Number of addition steps in multiplication */
static int addMultSteps(char * aString, char * bString)
{
	/* 2 digits per multiplication */
	return 2 * multSteps(aString, bString);
}

/* Finds out number of division steps */
/* This can be better as answers with 0s have to many ops */
static int divSteps(AbacusWidget w, char * aString, char * bString,
		int decimalPosition)
{
	double a, b, c, threshold = 1.0;
	int count = 0, i;

	if (strlen(aString) == 0 || strlen(bString) == 0)
		return 0;
	a = convertToDecimal(w->abacus.base, aString);
	b = convertToDecimal(w->abacus.base, bString);
	if (b == 0.0)
		return 0;
	c = a / b;
	for (i = 0; i < decimalPosition; i++)
		threshold /= w->abacus.base;
	if (c < threshold)
		return 0;
	if (c >= 1.0) {
		while (c >= 1.0) {
			c /= w->abacus.base;
			count++;
		}
	} else {
		while (c < 1.0) {
			c *= w->abacus.base;
			count--;
		}
		count++;
	}
	return count;
}

/* Number of multiplying steps in division */
static int multDivSteps(AbacusWidget w, char * aString, char * bString)
{
	/* decimal included */
	return divSteps(w, aString, bString, w->abacus.decimalPosition) *
		((int) strlen(bString) - 1);
}

/* Number of subtraction steps for each multiplication in division */
static int subMultDivSteps(AbacusWidget w, char * aString, char * bString)
{
	/* 2 digits per multiplication */
	return 2 * multDivSteps(w, aString, bString);
}

/* Finds head digits for division */
static double headDividend(AbacusWidget w, char * string, int len)
{
	char newString[512];

	if (strlen(string) == 0)
		return 0.0;
	(void) strcpy(newString, string);
	shiftStringBuffer(newString, -len);
	return convertToDecimal(w->abacus.base, newString);
}

/* Finds head digits for division */
/* Probably will not keep since could overflow if not BigDecimal */
static double headDivisor(AbacusWidget w, char * string)
{
	if (strlen(string) == 0)
		return 0.0;
	return convertToDecimal(w->abacus.base, string);
}

/* Divide string into groups, size of group depends on root */
static void rootGroup(AbacusWidget w, char* string, int root, int** group)
{
	int decimal = getDecimalStringPosition(string);
	int length = (int) strlen(string);
	int nIntegral = decimal;
	int nDecimal = length - decimal - 1;
	int i, j, k, b, n;

	if (nIntegral == 1 && string[0] == '0')
		nIntegral = 0;
	nIntegral = (nIntegral + root - 1) / root;
	nDecimal = (nDecimal + root - 1) / root;
#ifdef DEBUG
	(void) printf("rootGroup: nIntegral %d, nDecimal %d\n",
		nIntegral, nDecimal);
#endif
	n = nIntegral + nDecimal + 1;
	if (*group != NULL)
		free(*group);
	*group = (int *) malloc(sizeof (int) * (size_t) (n + 1));
	for (i = 0; i < nIntegral; i++) {
		k = i * root + (decimal + root - 1) % root;
		(*group)[i] = 0;
		b = 1;
		for (j = 0; j < root; j++) {
			if (k - j >= 0)
				(*group)[i] += b * CHAR_TO_DIGIT(string[k - j]);
			else
				break;
			b *= w->abacus.base;
		}
	}
	(*group)[nIntegral] = -1;
	for (i = 0; i < nDecimal; i++) {
		k = i * root + 1 + decimal;
		(*group)[i + nIntegral + 1] = 0;
		b = 1;
		for (j = 0; j < root; j++) {
			if (k + root - 1 - j < length)
				(*group)[i + nIntegral + 1] += b *
					CHAR_TO_DIGIT(string[k + root - 1 - j]);
			b *= w->abacus.base;
		}
	}
	(*group)[n] = -2;
}

#ifdef DEBUG
#ifdef KOJIMA_ROOT
/* Implement an iteration of Newton's Method or Halley's Method */
static double rootAdvance(double x, double p, int n)
{
	int i = 0;
	double y = x;

	if (n < 1)
		return 0;
	if (n == 1)
		return x;
	/* Halley's Method, converges faster */
	if (n == 2) {
		y = x * x;
#ifdef DEBUG
		(void) printf("p %g, y %g, n %d, x %g\n", p, y, n, x);
#endif
		return x * (y + p * 3) / (y * 3 + p);
	}
	if (n == 3) {
		y = x * x * x;
#ifdef DEBUG
		(void) printf("p %g, y %g, n %d, x %g\n", p, y, n, x);
#endif
		return x * (y + p * 2) / (y * 2 + p);
	}
	/* Newton's Method */
	for (i = 0; i < n - 2; i++)
		y *= x;
#ifdef DEBUG
	(void) printf("p %g, y %g, n %d, x %g\n", p, y, n, x);
#endif
	return (p / y + x * (n - 1)) / n;
}

/* Calculate root, loop a number of times and try for certain approximation */
static double rootApprox(double guess, double power, int n)
{
	int i;
	double xi = guess;
	const int iterations = 8;

	for (i = 0; i < iterations; i++) {
		double xn = rootAdvance(xi, power, n);
		double diff = xn - xi;

#ifdef DEBUG
		(void) printf("%d: %g %g %g\n", i, xn, xi, diff);
#endif
		if (diff < 0.0)
			diff = -diff;
		if (diff < EPSILON)
			break;
		xi = xn;
	}
	return xi;
}

/* This calculates first digit in a root */
static void testRootKojima(AbacusWidget w, double guess, int position, int root)
{
	double c = convertToDecimal(w->abacus.base, w->abacus.cString);
	int aPlace = position;
	double start = guess;

	while (aPlace > 1) {
		start *= w->abacus.base;
		aPlace--;
	}
	while (aPlace < 0 && start != 0.0) {
		start = w->abacus.base / start;
		aPlace++;
	}
	c = rootApprox(guess, c, root);
#ifdef DEBUG
	(void) printf("incr root = %g\n", c);
#endif
}
#endif

/* Figure out next intGroup value */
static int intGroupValue(AbacusWidget w, int position) {
	int i, newPosition = position;

	if (position < 0)
		return 0;
	if (position >= arrayLen(w->abacus.intGroup))
		return 0;
	for (i = 0; i <= position; i++) {
		if (w->abacus.intGroup[i] == -1) { /* only one decimal point */
			newPosition = position + 1;
			break;
		}
	}
	if (newPosition >= arrayLen(w->abacus.intGroup))
		return 0;
	return w->abacus.intGroup[newPosition];
}

typedef struct {
	int digitAdd;
	int orderInc; /* cbrt only */
	int groupInc;
} digitStruct;

static int testRootAdvance(AbacusWidget w, int power, int root, int *digitSum,
		int *orderInc, int *orderSum, int *groupInc, int *groupSum)
{
	int digitAdd = 1;
	int i = 0;
	int doubleOrder = 0;

#ifdef DEBUG
	(void) printf("remainder power = %d\n", power);
#endif
	while (*groupSum + ((root == 2) ? *groupInc : *orderInc) <= power) {
		if (root == 2) {
			*groupInc += digitAdd;
		} else {
			*orderInc += digitAdd;
			if (i == 0)
				doubleOrder = digitAdd;
			else
				doubleOrder += *orderInc;
			*groupInc += doubleOrder;
		}
#ifdef DEBUG
		(void) printf("%d: left A-0 field %d, ", (i + 1), digitAdd);
		if (root == 3)
			(void) printf("right A-O field %d, ", *orderInc);
		(void) printf("P-O field %d\n", *groupInc);
#endif
		i++;
		*digitSum += digitAdd;
		*groupSum += *groupInc;
		if (root == 3) {
			*orderSum += *orderInc;
			digitAdd = 1;
			*orderInc += digitAdd;
			doubleOrder = *orderInc;
#ifdef DEBUG
			(void) printf("%d: left A-0 field %d, right A-O field %d\n",
				(i + 1), digitAdd, *orderInc);
#endif
			i++;
			*digitSum += digitAdd;
			*orderSum += *orderInc;
		}
		digitAdd = 2;
	}
#ifdef DEBUG
	(void) printf("sum %d: %d", i, *digitSum);
	if (root == 3)
		(void) printf(" : %d %d", *orderInc, *orderSum);
	(void) printf(" : %d %d\n", *groupInc, *groupSum);
#endif
	return i;
}

/* This will test Lee's method for calculating roots via Newton's Method */
static void testRootLee(AbacusWidget w, int position, int root)
{
	int i, power = 0, j;
	int digitSum = 0;
	int orderInc = 0, orderSum = 0, groupInc = 0, groupSum = 0;
	int basePow = w->abacus.base * w->abacus.base *
		((root == 2) ? 1 : w->abacus.base);
	int magnitude = w->abacus.base;
	int allDigitSum = 0, groupVal = 0;
	double result = 0.0;

	for (i = 0; i <= position + w->abacus.decimalPosition; i++) {
		groupVal = intGroupValue(w, i);
		power = (power - groupSum) * basePow + groupVal;
		digitSum = 0;
		groupSum = 0;
#ifdef DEBUG
		(void) printf("left A-0 digit %d: incr power = %d at position %d, intGroup %d\n",
			(i + 1), power, position, groupVal);
#endif
		digitSum *= w->abacus.base;
		if (root == 3)
			orderSum *= w->abacus.base;
		groupSum *= basePow;
		j = testRootAdvance(w, power, root, &digitSum,
			&orderInc, &orderSum, &groupInc, &groupSum);
		if (j == 0) {
			digitSum = 0;
			if (root == 2)
				groupInc = groupInc * w->abacus.base;
			else
				orderInc = orderInc * w->abacus.base;
		} else {
			digitSum++;
			if (root == 2)
				groupInc = (groupInc + 1) * w->abacus.base;
			else
				orderInc = (orderInc + 1) * w->abacus.base;
		}
		if (root == 3) {
			magnitude *= w->abacus.base;
			groupInc = orderSum * magnitude + orderInc;
		}
#ifdef DEBUG
		(void) printf("left A-0 digit %d: digitSum = %d, ",
			(i + 1), digitSum);
		if (root == 3)
			(void) printf("orderSum = %d, ", orderSum);
		(void) printf("groupSum = %d, power = %d, j = %d\n",
			groupSum, power, j);
#endif
		allDigitSum = allDigitSum * w->abacus.base + digitSum;
	}
	allDigitSum--;
	result = (double) (allDigitSum + root - 1) / root;
#ifdef DEBUG
	(void) printf("allDigitSum %d, result = %g\n",
		allDigitSum, result);
#endif
	for (i = 0; i < w->abacus.decimalPosition; i++) {
		result /= w->abacus.base;
	}
#ifdef DEBUG
	(void) printf("result = %g\n", result);
#endif
}
#endif

static void
showHighlightRail(AbacusWidget w, int aux, int rail, Boolean highlight)
{
#ifndef TEST
	hOn = highlight;
	hAux = aux;
	hRail = rail;
	highlightRail(w, aux, rail, highlight);
#endif
}

static void
showHighlightRails(AbacusWidget w, int aux)
{
#ifndef TEST
	highlightRails(w, aux);
#endif
}

/* Tell about what is going to happen */
/* extra space at end is to get rid of [] bug */
static Boolean pendingUpdate(AbacusWidget w, char * buffer,
	int line, int aux, int position, int base, int bottomNumber)
{
	Boolean done;
#ifdef OLD_DEBUG
	(void) printf("pendingUpdate %s, %d, %d, %d, %d, %d, %d, %d\n",
		buffer, line, aux, position, base, bottomNumber,
		w->abacus.lower, w->abacus.upper);
#endif

	(void) sprintf(buffer, "For rail %d", position);
	if (w->abacus.lower == 0 && w->abacus.upper == 0 &&
			w->abacus.carry[w->abacus.state] == 0) {
		(void) strncat(buffer, " do nothing", 12);
		w->abacus.step++; /* or else two do nothings */
		done = True;
	} else {
		showHighlightRail(w, aux, position, True);
		if (w->abacus.lower != 0) {
			if (w->abacus.lower < 0)
				(void) strncat(buffer, ", take off ", 12);
			else
				(void) strncat(buffer, ", put on ", 10);
			strAddInt(buffer, ABS(w->abacus.lower));
			if (bottomNumber <= base / 2) {
				if (w->abacus.lower < 0)
					(void) strncat(buffer, " from lower deck", 17); 
				else
					(void) strncat(buffer, " to lower deck", 15);
			}
		}
		if (w->abacus.upper != 0) {
			if (w->abacus.upper < 0)
				(void) strncat(buffer, ", take off ", 12);
			else
				(void) strncat(buffer, ", put on ", 10);
			strAddInt(buffer, ABS(w->abacus.upper));
			if (w->abacus.upper < 0)
				(void) strncat(buffer, " from upper deck", 17);
			else
				(void) strncat(buffer, " to upper deck", 15);
		}
		if (w->abacus.carry[w->abacus.state] != 0) {
			if (w->abacus.state > 0)
				(void) strncat(buffer, ", carry ", 9);
			else
				(void) strncat(buffer, ", borrow ", 10);
			strAddInt(buffer, ABS(w->abacus.carry[w->abacus.state]));
			(void) strncat(buffer, " (on next move)", 16);
		}
		done = False;
	}
	(void) strncat(buffer, ". ", 3);
	drawLineText(w, buffer, line);
	return done;
}

static int decimalPlaceToRailPosition(AbacusWidget w, int aux, int decimalPlace) {
	AbacusWidget *wap = &w, wa;
	int shiftPercent;
	Boolean piece, piecePercent, subdeck;
	int rail = decimalPlace;

	if (aux == 1 && w->abacus.leftAux != NULL)
		wap = (AbacusWidget *) w->abacus.leftAux;
	else if (aux == 2 && w->abacus.rightAux != NULL)
		wap = (AbacusWidget *) w->abacus.rightAux;
	wa = *wap;
	shiftPercent = wa->abacus.shiftPercent;
	piece = checkPiece(wa);
	piecePercent = checkPiecePercent(wa);
	subdeck = checkSubdeck(wa, 3);
	if (subdeck) {
		if (decimalPlace < 0)
			rail -= 3;
	} else {
		if (piece && (decimalPlace < 0))
			rail--;
		if (piecePercent && (decimalPlace < -shiftPercent))
			rail--;
	}
#ifdef DEBUG
	(void) printf("decimalPlaceToRailPosition: %d railPosition, %d decimalPlace\n",
		rail, decimalPlace);
#endif
	return rail;
}

static int decimalOffset(AbacusWidget w, int aux) {
	AbacusWidget *wap = &w, wa;
	Boolean piece, piecePercent, subdeck;
	int decimalPosition;

	if (aux == 1 && w->abacus.leftAux != NULL)
		wap = (AbacusWidget *) w->abacus.leftAux;
	else if (aux == 2 && w->abacus.rightAux != NULL)
		wap = (AbacusWidget *) w->abacus.rightAux;
	wa = *wap;
	decimalPosition = wa->abacus.decimalPosition;
	piece = checkPiece(wa);
	piecePercent = checkPiecePercent(wa);
	subdeck = checkSubdeck(wa, 3);
	if (subdeck) {
		decimalPosition -= 3;
	} else {
		if (piece)
			decimalPosition--;
		if (piecePercent)
			decimalPosition--;
	}
	return decimalPosition;
}

/* Handle addition and subtraction one step at a time */
static Boolean nextPositionSum(AbacusWidget w, char op,
		int *dPosition, int *rDigit, int *bDigit)
{
	int n = (w->abacus.step - 2) / 2; /* 2 step display */
	int max = addSteps(w->abacus.aString); /* number of steps with original */
	int topFactor = w->abacus.decks[TOP].factor;
	int bottomNumber = w->abacus.decks[BOTTOM].number;
	Boolean rightToLeft = w->abacus.rightToLeftAdd;
	int rPos, bPos, place;

	/* rString can expand with carries. */
	/* bString does not change, so bPos will be predictable. */
	bPos = nextCharPosition(n, w->abacus.carryStep,
		rightToLeft, w->abacus.bString);
	place = decimalPlaceString(w->abacus.bString, bPos);
	rPos = decimalOffsetString(w->abacus.rString, place);
	*rDigit = nextChar(w->abacus.rString, rPos);
	if (w->abacus.carryStep == 0)
		*bDigit = nextChar(w->abacus.bString, bPos);
	else
		*bDigit = 1;
	if (!rightToLeft)
		w->abacus.carry[w->abacus.state] = 0;
	if (op == '+')
		digitAdd(*rDigit, *bDigit,
			w->abacus.carry[w->abacus.state],
			w->abacus.base, topFactor, bottomNumber,
			&(w->abacus.lower), &(w->abacus.upper),
			&(w->abacus.carry[w->abacus.state]));
	else
		digitSubtract(*rDigit, *bDigit,
			w->abacus.carry[w->abacus.state],
			w->abacus.base, topFactor, bottomNumber,
			&(w->abacus.lower), &(w->abacus.upper),
			&(w->abacus.carry[w->abacus.state]));
	*dPosition = place;
#ifdef DEBUG
	(void) printf("nextPositionSum %d dPosition, %d rDigit, %d bDigit, %s bString, %d Pos, %s rString\n",
		*dPosition, *rDigit, *bDigit, w->abacus.bString, bPos, w->abacus.rString);
#endif
	return (n >= max - 1);
}

/* Handle multiplication one step at a time */
static Boolean nextPositionProduct(AbacusWidget w,
		int *dPosition, int *aDigit, int *bDigit, int *bValue)
{
	int n = (w->abacus.step - 2) / 2; /* 2 step display */
	int max = addMultSteps(w->abacus.aString, w->abacus.bString);
	int topFactor = w->abacus.decks[TOP].factor;
	int bottomNumber = w->abacus.decks[BOTTOM].number;
	int bCount = (int) strlen(w->abacus.bString) - 1;
	Boolean rightToLeft = w->abacus.rightToLeftAdd;
	int aOffset, bOffset, rOffset;
	int aPlace, bPlace, rPlace, rDigit;

	aOffset = nextCharPosition(
		(n / 2) / bCount, /* 2 digits result for each multiplication */
		0, /* not place for carry */
		True, /* "a" side always starts on right */
		w->abacus.aString);
	aPlace = decimalPlaceString(w->abacus.aString, aOffset);
	*aDigit = nextChar(w->abacus.aString, aOffset);
	bOffset = nextCharPosition(
		(n / 2) % bCount, /* 2 digits result for each multiplication */
		0, /* not place for carry */
		w->abacus.rightToLeftMult, /* this can vary */
		w->abacus.bString);
	bPlace = decimalPlaceString(w->abacus.bString, bOffset);
	*bDigit = nextChar(w->abacus.bString, bOffset);
	rPlace = (((n & 1) == 0 && !rightToLeft) ||
		((n & 1) == 1 && rightToLeft)) ? 1 : 0;
	if (w->abacus.carryStep != 0) {
		rPlace += w->abacus.carryStep / 2;
		*bDigit = 1;
	}
	rPlace += aPlace + bPlace;
	expandStringBuffer(w->abacus.rString, rPlace);
	rOffset = decimalOffsetString(w->abacus.rString, rPlace);
	rDigit = nextChar(w->abacus.rString, rOffset);
	if (w->abacus.carryStep == 0) {
		/* 2 digits * 2 step display */
		if ((((n & 1) == 0) && rightToLeft) ||
				(((n & 1) == 1) && !rightToLeft)) {
			*bValue = (*aDigit * *bDigit) % w->abacus.base;
		} else {
			*bValue = (*aDigit * *bDigit) / w->abacus.base;
		}
	} else {
		*aDigit = 0;
		*bDigit = 0;
		*bValue = 1;
	}
	if (!rightToLeft)
		w->abacus.carry[w->abacus.state] = 0;
	digitAdd(rDigit, *bValue,
		w->abacus.carry[w->abacus.state],
		w->abacus.base, topFactor, bottomNumber,
		&(w->abacus.lower), &(w->abacus.upper),
		&(w->abacus.carry[w->abacus.state]));
	*dPosition = rPlace;
	if (w->abacus.carry[w->abacus.state] != 0 && ((n & 1) == 1) &&
			rightToLeft)
		return True; /* Do not want to forget carry */
	return (n >= max - 1);
}

/* Handle division one step at a time */
static Boolean nextPositionDivision(AbacusWidget w,
		int *dPosition, int *aDigit, int *bDigit, int *bValue)
{
	int n = (w->abacus.step - 2) / 2; /* 2 step display */
	int max = subMultDivSteps(w, w->abacus.aString,
		w->abacus.bString);
	int topFactor = w->abacus.decks[TOP].factor;
	int bottomNumber = w->abacus.decks[BOTTOM].number;
	int bCount = (int) strlen(w->abacus.bString) - 1;
	Boolean rightToLeft = w->abacus.rightToLeftAdd;
	int bOffset, rOffset;
	int aPlace, bPlace, rPlace, rDigit;
	double dividend;

	if (n / (2 * bCount) == w->abacus.regCount / 2) {
		w->abacus.reg = w->abacus.regCount;
	} else {
		w->abacus.reg = -1;
	}
	if (w->abacus.reg >= 0) {
		AbacusWidget *wap = &w, wa;

		if (w->abacus.rightAux != NULL)
			wap = (AbacusWidget *) w->abacus.rightAux;
		wa = *wap;
		if (w->abacus.reg > 1)
			(void) strcpy(w->abacus.cString, w->abacus.rString);
		w->abacus.regCount++;
		aPlace = divSteps(w, w->abacus.cString, w->abacus.bString,
			wa->abacus.decimalPosition) - 1;
#ifdef DEBUG
		{
			int aOffset = decimalPlaceString(w->abacus.cString,
				aPlace);
			(void) printf("aPlace = %d, aOffset = %d\n",
				aPlace, aOffset);
		}
#endif
		dividend = headDividend(w, w->abacus.cString, aPlace);
		w->abacus.divisor = headDivisor(w, w->abacus.bString);
#ifdef DEBUG
		(void) printf("dividend = %g, divisor = %g\n",
			dividend, w->abacus.divisor);
#endif
		if (w->abacus.divisor == 0)
			w->abacus.qDigit = 0;
		else {
			w->abacus.qDigit = (int) (dividend / w->abacus.divisor +
				EPSILON);
		}
#ifdef DEBUG
		(void) printf("cString = %s, qDigit = %d\n",
			w->abacus.cString, w->abacus.qDigit);
#endif
		w->abacus.qPosition = aPlace;
		digitAdd(0, w->abacus.qDigit, w->abacus.carry[w->abacus.state],
			wa->abacus.base, wa->abacus.decks[TOP].factor, wa->abacus.decks[BOTTOM].number,
			&(w->abacus.lower), &(w->abacus.upper),
			&(w->abacus.carry[w->abacus.state]));
		return (n >= 2 * max * 2 * w->abacus.decimalPosition - 1);
	}
	bOffset = nextCharPosition(
		(n / 2) % bCount, /* 2 digits result for each multiplication */
		0, /* not place for carry */
		w->abacus.rightToLeftMult, /* this can vary */
		w->abacus.bString);
	bPlace = decimalPlaceString(w->abacus.bString, bOffset);
	*bDigit = nextChar(w->abacus.bString, bOffset);
	rPlace = ((((n & 1) == 0) && !rightToLeft) ||
		(((n & 1) == 1) && rightToLeft)) ? 1 : 0;
	if (w->abacus.carryStep != 0) {
		rPlace += w->abacus.carryStep / 2;
		*bDigit = 1;
	}
	rPlace += w->abacus.qPosition + bPlace;
	expandStringBuffer(w->abacus.rString, rPlace);
	rOffset = decimalOffsetString(w->abacus.rString, rPlace);
	rDigit = nextChar(w->abacus.rString, rOffset);
#ifdef OLD_DEBUG
	(void) printf("bOffset %d, bPlace %d, bDigit %d\n",
		bOffset, bPlace, *bDigit);
#endif
	if (w->abacus.carryStep == 0) {
		/* 2 digits * 2 step display */
		if ((((n & 1) == 0) && rightToLeft) ||
				(((n & 1) == 1) && !rightToLeft)) {
			*bValue = (w->abacus.qDigit * *bDigit) % w->abacus.base;
		} else {
			*bValue = (w->abacus.qDigit * *bDigit) / w->abacus.base;
		}
	} else {
		*aDigit = 0;
		*bDigit = 0;
		*bValue = 1;
	}
	if (!rightToLeft)
		w->abacus.carry[w->abacus.state] = 0;
	digitSubtract(rDigit, *bValue, w->abacus.carry[w->abacus.state],
		w->abacus.base, topFactor, bottomNumber,
		&(w->abacus.lower), &(w->abacus.upper),
		&(w->abacus.carry[w->abacus.state]));
	*dPosition = rPlace;
	return (n >= 2 * max + 2 * w->abacus.decimalPosition - 1);
}

/* Handle root one step at a time, Kojima */
static Boolean nextPositionRoot(AbacusWidget w, int root,
		int *dPosition, int *aDigit, int *bDigit, int *bValue)
{
	int n = (w->abacus.step - 2) / 2; /* 2 step display */
	int max = subMultDivSteps(w, w->abacus.aString,
		w->abacus.bString);
	int aOffset, aPlace;
	int power;

	if (w->abacus.reg >= 0) {
		AbacusWidget *wap = &w, wa;

		if (w->abacus.leftAux != NULL)
			wap = (AbacusWidget *) w->abacus.leftAux;
		wa = *wap;
		if (w->abacus.reg > 1)
			(void) strcpy(w->abacus.cString, w->abacus.rString);
		w->abacus.regCount++;
		aOffset = 0;
		if (w->abacus.intGroup[aOffset] == -1)
			aOffset++;
		aPlace = decimalPlaceArray(w->abacus.intGroup, aOffset);
#ifdef DEBUG
		(void) printf("aPlace = %d, aOffset = %d\n", aPlace, aOffset);
#endif
		if (w->abacus.intGroup[aOffset] == -1)
			power = 0;
		else
			power = w->abacus.intGroup[aOffset];
		w->abacus.qDigit = rootInt(power, root);
#ifdef DEBUG
		(void) printf("power = %d\n", power);
		(void) printf("cString = %s, qDigit = %d\n",
			w->abacus.cString, w->abacus.qDigit);
#endif
		w->abacus.qPosition = aPlace;
#ifdef DEBUG
#ifdef KOJIMA_ROOT
		testRootKojima(w, w->abacus.qDigit, aPlace, root);
#endif
		testRootLee(w, aPlace, root);
#endif
		digitAdd(0, w->abacus.qDigit, w->abacus.carry[w->abacus.state],
			wa->abacus.base, wa->abacus.decks[TOP].factor, wa->abacus.decks[BOTTOM].number,
			&(w->abacus.lower), &(w->abacus.upper),
			&(w->abacus.carry[w->abacus.state]));
		return (n >= 2 * max * 2 * w->abacus.decimalPosition - 1);
	}
	/* Only figures out first digit so far */
	/* May want to fix with (a+b)(a+b) = a^2+2ab+b^2 for 2 digits */
	/* and (a+b+c)(a+b+c) = a^2+2ab+b^2+2bc+c^2+2ac for 3 digits */
	/* see Advanced Abacus by Takashi Kojima */
	w->abacus.lower = 0;
	w->abacus.upper = 0;
	/* reserved for future */
	*dPosition = 0;
	*aDigit = 0;
	*bDigit = 0;
	*bValue = 0;
	return True;
}

#ifdef LEE_ROOT
/* Handle Left Auxiliary Operations Field table one step at a time */
static Boolean nextPositionLAOField(AbacusWidget w,
		int *dPosition, int *bDigit, int count)
{
	int topFactor = w->abacus.decks[TOP].factor;
	int bottomNumber = w->abacus.decks[BOTTOM].number;
	int root = (w->abacus.op == 'v') ? 2 : 3;
	int oFPos, oFDigit;

	if (w->abacus.leftAux != NULL) {
		AbacusWidget *wap = &w, wa;

		wap = (AbacusWidget *) w->abacus.leftAux;
		wa = *wap;
		topFactor = wa->abacus.decks[TOP].factor;
		bottomNumber = wa->abacus.decks[BOTTOM].number;
	}
	*dPosition = w->abacus.regCount;
	if (w->abacus.carryStep != 0)
		*dPosition += w->abacus.carryStep / 2;
	oFPos = decimalOffsetString(w->abacus.bString, *dPosition);
	oFDigit = nextChar(w->abacus.bString, oFPos);
	if (w->abacus.carryStep == 0) {
		if (root == 2)
			*bDigit = 1 + ((count > 0) ? 1 : 0);
		else
			*bDigit = 1 + ((count > 0 &&
				((count & 1) == 0)) ? 1 : 0);
	} else {
		*bDigit = 1;
	}
	w->abacus.carry[w->abacus.state] = 0;
	digitAdd(oFDigit, *bDigit, w->abacus.carry[w->abacus.state],
		w->abacus.base, topFactor, bottomNumber,
		&(w->abacus.lower), &(w->abacus.upper),
		&(w->abacus.carry[w->abacus.state]));
#ifdef DEBUG
	(void) printf("LAOF: bs %s, bDigit %d, oFPos %d, oFDigit %d, dPosition %d, carryStep %d\n",
w->abacus.bString, *bDigit, oFPos, oFDigit, *dPosition, w->abacus.carryStep);
#endif
	return True; /* FIXME? */
}

/* Handle Right Auxiliary Operations Field table one step at a time */
/* Only used for cube root */
static Boolean nextPositionRAOField(AbacusWidget w,
		int *dPosition, int *bDigit, int count, int steps)
{
	int topFactor = w->abacus.decks[TOP].factor;
	int bottomNumber = w->abacus.decks[BOTTOM].number;
	Boolean rightToLeft = w->abacus.rightToLeftAdd;
	int oFPos, oFDigit;
	int charPos, place;
	char buffer[120] = "";

	if (w->abacus.rightAux != NULL) {
		AbacusWidget *wap = &w, wa;

		wap = (AbacusWidget *) w->abacus.rightAux;
		wa = *wap;
		topFactor = wa->abacus.decks[TOP].factor;
		bottomNumber = wa->abacus.decks[BOTTOM].number;
	}
	/* buffer is bString or shorter */
	(void) strcpy(buffer, w->abacus.bString);
	stripStringBuffer(buffer, w->abacus.regCount, steps);
	*dPosition = 2 * w->abacus.regCount;
/*printf("BEFORE dPosition %d, regCount %d, buffer %s, bString %s %d %d\n", *dPosition, w->abacus.regCount, buffer, w->abacus.bString, strlen(buffer), strlen(w->abacus.bString));*/
	charPos = nextCharPosition(count, w->abacus.carryStep,
		rightToLeft, buffer);
	place = decimalPlaceString(buffer, charPos);
	*dPosition += place;
/*printf("AFTER dPostition %d, place %d, string %s, charPos%d\n", *dPosition, place, buffer, charPos);*/
	oFPos = decimalOffsetString(w->abacus.cString, *dPosition);
	oFDigit = nextChar(w->abacus.cString, oFPos);
	if (w->abacus.carryStep == 0) {
		*bDigit = nextChar(buffer, charPos);
	} else {
		*bDigit = 1;
	}
	if (!rightToLeft)
		w->abacus.carry[w->abacus.state] = 0;
	digitAdd(oFDigit, *bDigit, w->abacus.carry[w->abacus.state],
		w->abacus.base, topFactor, bottomNumber,
		&(w->abacus.lower), &(w->abacus.upper),
		&(w->abacus.carry[w->abacus.state]));
#ifdef DEBUG
	(void) printf("RAOF: buffer %s, bs %s, cs %s, oFPos %d, oFDigit %d, dPosition %d, bDigit %d, count %d, place %d, carry %d, carryStep %d, regCount %d, charPos%d\n",
buffer, w->abacus.bString, w->abacus.cString, oFPos, oFDigit, *dPosition, *bDigit, count, place, w->abacus.carry[w->abacus.state], w->abacus.carryStep, w->abacus.regCount, charPos);
#endif
	return True; /* FIXME? */
}

/* Handle Primary Operations Field table one step at a time */
static Boolean nextPositionPOField(AbacusWidget w,
		int *dPosition, int *bDigit, int count)
{
	int topFactor = w->abacus.decks[TOP].factor;
	int bottomNumber = w->abacus.decks[BOTTOM].number;
	Boolean rightToLeft = w->abacus.rightToLeftAdd;
	int oFPos, oFDigit;
	int root = (w->abacus.op == 'v') ? 2 : 3;
	int charPos, place;
	char buffer[120] = "";

	/* buffer is the string or shorter */
	if (root == 2)
		(void) strcpy(buffer, w->abacus.bString);
	else
		(void) strcpy(buffer, w->abacus.cString);
	stripStringBuffer(buffer, (root - 1) * w->abacus.regCount, 0);
	*dPosition = root * w->abacus.regCount;
	charPos = nextCharPosition(count, w->abacus.carryStep,
		rightToLeft, buffer);
	place = decimalPlaceString(buffer, charPos);
	*dPosition += place;
	/* rString can expand with carries. */
	oFPos = decimalOffsetString(w->abacus.rString, *dPosition);
	oFDigit = nextChar(w->abacus.rString, oFPos);
	if (w->abacus.carryStep == 0) {
		*bDigit = nextChar(buffer, charPos);
	} else {
		*bDigit = 1;
	}
	if (!rightToLeft)
		w->abacus.carry[w->abacus.state] = 0;
	digitSubtract(oFDigit, *bDigit, w->abacus.carry[w->abacus.state],
		w->abacus.base, topFactor, bottomNumber,
		&(w->abacus.lower), &(w->abacus.upper),
		&(w->abacus.carry[w->abacus.state]));
#ifdef DEBUG
	(void) printf("POF: rs %s, buffer %s, oFPos %d, oFDigit %d, dPosition %d, bDigit %d, count %d, place %d, carry %d, carryStep %d, reg %d, regCount %d, charPos %d\n",
		w->abacus.rString, buffer, oFPos, oFDigit, *dPosition, *bDigit, count, place, w->abacus.carry[w->abacus.state], w->abacus.carryStep, w->abacus.reg, w->abacus.regCount, charPos);
#endif
	return True; /* FIXME? */
}

/* A little tricky as this is reentrant */
static void teachStepRoot(AbacusWidget w)
{
	char buffer2[120] = "";
	char buffer3[120] = "";
	/*Boolean done = False;*/
	int bDigit = 0, bValue, cValue = 0;
	int dPosition = 0, rPosition = 0, rValue;
	int rStep = w->abacus.step - 2;
	int bSteps = 0, nSteps; /* number of steps in interation */
	int root = (w->abacus.op == 'v') ? 2 : 3;
	double r = convertToDecimal(w->abacus.base, w->abacus.rString);
	double a = convertToDecimal(w->abacus.base, w->abacus.aString);
	int futureOffset = 1;
	int decimalPosition = decimalOffset(w, 0);
	int decimalPosition1 = decimalOffset(w, 1);
	int decimalPosition2 = decimalOffset(w, 2);
	int aux;

	if (w->abacus.reg == -1) {
		w->abacus.regCount = placeGroup(w->abacus.intGroup) - 1;
		w->abacus.reg = 0;
		w->abacus.primarySteps = 2 * andAbovePlaces(w->abacus.rString,
			root * w->abacus.regCount);
	}
	rValue = andAbove(w->abacus.rString, root * w->abacus.regCount,
		w->abacus.base);
	bValue = andAbove(w->abacus.bString, w->abacus.regCount,
		w->abacus.base);
	nSteps = 4 + w->abacus.primarySteps;
	if (root == 3) {
		cValue = andAbove(w->abacus.cString, 2 * w->abacus.regCount,
			w->abacus.base);
		bSteps = 2 *
			andAbovePlaces(w->abacus.bString, w->abacus.regCount);
	}
	if (rStep / nSteps == 0) {
		futureOffset = 0;
	}
#ifdef DEBUG
	(void) printf("teachStepRoot: rString%s, bString%s, cString%s, rValue%d, bValue%d, cValue%d, reg%d, regCount%d, auxCount%d, carryStep%d, rStep%d, bSteps%d, nSteps%d, r%g, a%g, decimalPosition %d, decimalPosition1 %d, decimalPosition2 %d\n",
		w->abacus.rString, w->abacus.bString, w->abacus.cString, rValue, bValue, cValue, w->abacus.reg, w->abacus.regCount, w->abacus.auxCount, w->abacus.carryStep, rStep, bSteps, nSteps, r, a, decimalPosition, decimalPosition1, decimalPosition2);
#endif
	if (rStep % nSteps == 0) {
		if ((r == 0.0 && r < a) ||
				((root == 2) && bValue + futureOffset >= rValue) ||
				((root == 3) && cValue + (2 * bValue + 3) * futureOffset >= rValue &&
				(((rStep / nSteps) % 2) != 0) &&
				(w->abacus.auxCount >= bSteps || w->abacus.auxCount == 0))) {
			if (w->abacus.reg < 1)
				w->abacus.reg = 1; /* need for carry as bValue changes */
		}
		if (w->abacus.reg >= 1) {
			if (futureOffset == 0) {
				/* Have not started with this position and
				   nothing here */
				w->abacus.step = 2;
				w->abacus.regCount--;
				w->abacus.primarySteps = 2 *
					andAbovePlaces(w->abacus.rString, root * w->abacus.regCount);
				w->abacus.reg = 0;
				if ((root == 3 && w->abacus.regCount < -decimalPosition2 / (2 * (root - 1))) ||
						w->abacus.regCount < -decimalPosition1 ||
						w->abacus.regCount < -decimalPosition / ((root - 1) * root)) {
					w->abacus.step = 0;
					(void) sprintf(buffer3, "Answer (divide by %d): %s",
						root, w->abacus.bString);
					drawLineText(w, buffer3, 2);
					(void) strncpy(buffer2, "Done", 5);
					showHighlightRails(w, 1);
				} else {
					(void) strncpy(buffer2, "Now try smaller value", 22);
				}
			} else {
				(void) strncpy(buffer2,
					"Almost done with position, need to add 1 to place working on", 61);
				w->abacus.step += 2;
			}
			drawLineText(w, buffer2, 1);
		} else {
			(void) sprintf(buffer2,
				"%s A-O field value %d%s < P-O field group value %d",
				((root == 2) ? "Left" : "Right"),
				((root == 2) ? bValue : cValue),
				((futureOffset != 0) ? " + 1" : ""),
				rValue);
			drawLineText(w, buffer2, 1);
			w->abacus.step++;
		}
	} else if (rStep % nSteps == 1) {
		if (((root == 2) ? bValue : cValue) + futureOffset < rValue) {
			(void) sprintf(buffer2, "Yes, ok to iterate");
			w->abacus.step++;
		} else {
			if (rValue == 0) {
				w->abacus.step = 0;
				(void) sprintf(buffer3, "Answer (divide by %d): %s",
					root, w->abacus.bString);
				drawLineText(w, buffer3, 2);
				(void) strncpy(buffer2, "Done", 5);
				showHighlightRails(w, 1);
			} else {
				(void) strncpy(buffer2, "Try smaller value", 18);
				w->abacus.step = 2;
				w->abacus.reg = 0;
				w->abacus.regCount--;
				w->abacus.primarySteps = 2 *
					andAbovePlaces(w->abacus.rString, root * w->abacus.regCount);
			}
		}
		drawLineText(w, buffer2, 1);
		w->abacus.carryStep = 0;
		w->abacus.carry[0] = 0;
		w->abacus.carry[1] = 0;
	} else if (rStep % nSteps == 2 ||
			(rStep % nSteps == 3 &&
			w->abacus.carryStep != 0 &&
			w->abacus.carryStep % 2 == 0)) {
		aux = 1;
		w->abacus.state = 0;
		/*done = */(void) nextPositionLAOField(w, &dPosition, &bDigit,
			((w->abacus.reg >= 1) ? root - 2 : rStep / nSteps));
		rPosition = decimalPlaceToRailPosition(w, aux, dPosition);
		showHighlightRail(w, aux, rPosition, True);
		if (w->abacus.carryStep != 0)
			(void) sprintf(buffer2,
				"Carrying, add %d to left A-O field, offset digit at position %d",
				bDigit, rPosition); /* should only need carry once */
		else
			(void) sprintf(buffer2,
				"Add %d to left A-O field, offset digit by corresponding group (or position) %d%s",
				bDigit, rPosition,
				((w->abacus.carry[w->abacus.state] != 0) ? ", with carry" : ""));
		drawLineText(w, buffer2, 1);
		if (w->abacus.carryStep == 0) {
			w->abacus.step++;
		} else {
			w->abacus.carryStep++;
		}
	} else if (rStep % nSteps == 3) {
		aux = 1;
		w->abacus.carry[1] = w->abacus.carry[0];
		w->abacus.state = 1;
		/*done = */(void) nextPositionLAOField(w, &dPosition, &bDigit,
			((w->abacus.reg >= 1) ? root - 2 : rStep / nSteps));
		rPosition = decimalPlaceToRailPosition(w, aux, dPosition);
		setStringBuffer(w, w->abacus.bString, aux,
			dPosition, w->abacus.lower, w->abacus.upper);
		if (w->abacus.lower != 0)
			setAbacusMove(w, ACTION_MOVE, aux, 0, rPosition,
				w->abacus.lower);
		if (w->abacus.upper != 0)
			setAbacusMove(w, ACTION_MOVE, aux, 1, rPosition,
				w->abacus.upper);
		showHighlightRail(w, aux, rPosition, False);
		if (w->abacus.carry[w->abacus.state] == 0 &&
				w->abacus.carryStep != 0) {
			w->abacus.carryStep = 0;
		}
		contractStringBuffer(w->abacus.bString);
		if (w->abacus.reg == root - 1) {
			if (w->abacus.carry[w->abacus.state] != 0) {
				if (w->abacus.carryStep == 0) {
					w->abacus.carryStep = 2;
				} else {
					w->abacus.carryStep++;
				}
				(void) sprintf(buffer3, "Current answer (divide by %d): %s ",
					root, w->abacus.bString);
				drawLineText(w, buffer3, 2);
			} else {
				w->abacus.step = 2;
				w->abacus.reg = 0;
				w->abacus.regCount--;
				w->abacus.primarySteps = 2 *
					andAbovePlaces(w->abacus.rString, root * w->abacus.regCount);
				if ((root == 3 && w->abacus.regCount < -decimalPosition2 / (2 * (root - 1))) ||
						w->abacus.regCount < -decimalPosition1 ||
						w->abacus.regCount < -decimalPosition / ((root - 1) * root)) {
					w->abacus.step = 0;
					(void) sprintf(buffer3, "Answer (divide by %d): %s",
						root, w->abacus.bString);
					drawLineText(w, buffer3, 2);
					showHighlightRails(w, 1);
				} else {
					(void) sprintf(buffer3, "Current answer (divide by %d): %s ",
						root, w->abacus.bString);
					drawLineText(w, buffer3, 2);
				}
			}
		} else {
			(void) sprintf(buffer3, "Current answer (divide by %d): %s ",
				root, w->abacus.bString);
			drawLineText(w, buffer3, 2);
			if (w->abacus.carry[w->abacus.state] != 0) {
				if (w->abacus.carryStep == 0) {
					w->abacus.carryStep = 2;
					if (w->abacus.rightToLeftAdd)
						w->abacus.carry[1] = w->abacus.carry[0] = 0;
				} else {
					w->abacus.carryStep++;
				}
			} else {
				w->abacus.carryStep = 0;
			}
			if (w->abacus.carryStep == 0) {
				w->abacus.step++;
				if (root == 3)
					w->abacus.auxCount = 0;
			}
		}
	} else if (root == 3 && w->abacus.auxCount < bSteps &&
			(w->abacus.auxCount % 2 == 0 ||
			(w->abacus.carryStep != 0 &&
			w->abacus.carryStep % 2 == 0))) {
		aux = 2;
		w->abacus.state = 0;
		/*done = */(void) nextPositionRAOField(w, &dPosition, &bDigit,
			w->abacus.auxCount / 2, bSteps / 2);
		rPosition = decimalPlaceToRailPosition(w, aux, dPosition);
		showHighlightRail(w, aux, rPosition, True);
		if (w->abacus.carryStep != 0)
			(void) strncpy(buffer2, "Carrying in right A-O field", 28);
		else
			if (bDigit != 0 && bDigit != bValue)
				(void) sprintf(buffer2,
					"Add %d (part of %d) to right A-O field, offset digit by corresponding position %d (in group %d)%s",
					bDigit, bValue, rPosition, w->abacus.regCount,
					((w->abacus.carry[w->abacus.state] != 0) ? ", with carry" : ""));
			else
				(void) sprintf(buffer2,
					"Add %d to right A-O field, offset digit by corresponding position %d (or group %d)%s",
					bDigit, rPosition, w->abacus.regCount,
					((w->abacus.carry[w->abacus.state] != 0) ? ", with carry" : ""));
		drawLineText(w, buffer2, 1);
		if (w->abacus.carryStep == 0) {
			w->abacus.auxCount++;
		} else {
			w->abacus.carryStep++;
		}
	} else if (root == 3 && w->abacus.auxCount < bSteps) {
		aux = 2;
		if (!w->abacus.rightToLeftAdd)
			w->abacus.carry[1] = w->abacus.carry[0];
		w->abacus.state = 1;
		/*done = */(void) nextPositionRAOField(w, &dPosition, &bDigit,
			w->abacus.auxCount / 2, bSteps / 2);
		rPosition = decimalPlaceToRailPosition(w, aux, dPosition);
		setStringBuffer(w, w->abacus.cString, aux,
			dPosition, w->abacus.lower, w->abacus.upper);
		if (w->abacus.lower != 0)
			setAbacusMove(w, ACTION_MOVE, aux, 0, rPosition,
				w->abacus.lower);
		if (w->abacus.upper != 0)
			setAbacusMove(w, ACTION_MOVE, aux, 1, rPosition,
				w->abacus.upper);
		showHighlightRail(w, aux, rPosition, False);
		if (w->abacus.carry[w->abacus.state] == 0 &&
				w->abacus.carryStep != 0) {
			w->abacus.carryStep = 0;
		}
		if (w->abacus.auxCount % bSteps != bSteps - 1 &&
				w->abacus.carry[w->abacus.state] == 0 &&
				logInt(bValue, w->abacus.base) <= w->abacus.auxCount / 2) {
			w->abacus.auxCount++;
		} else {
			if (w->abacus.carry[w->abacus.state] != 0) {
				if (w->abacus.carryStep == 0) {
					w->abacus.carryStep = 2;
				} else {
					w->abacus.carryStep++;
				}
			}
			if (w->abacus.rightToLeftAdd &&
				w->abacus.carry[w->abacus.state] != 0) {
				w->abacus.carry[1] = w->abacus.carry[0] = 0;
			}
			if (w->abacus.carryStep == 0) {
				w->abacus.auxCount++;
				if (w->abacus.auxCount == bSteps) {
					if (w->abacus.reg >= 1) {
						w->abacus.auxCount = 0;
						w->abacus.step = (w->abacus.step / nSteps + 1) * nSteps + 4;
						w->abacus.reg = 2;
					} else if ((rStep / nSteps) % 2 != 0) {
						/* want skip primary if odd */
						w->abacus.step = ((w->abacus.step - 2) / nSteps + 1) * nSteps + 2;
					}
				}
			}
		}
	} else if (rStep % 2 == 0 || (w->abacus.carryStep != 0 &&
			w->abacus.carryStep % 2 == 0)) {
		aux = 0;
		w->abacus.state = 0;
		/*done = */(void) nextPositionPOField(w, &dPosition, &bDigit,
			(rStep % nSteps - 4) / 2);
		rPosition = decimalPlaceToRailPosition(w, aux, dPosition);
		showHighlightRail(w, aux, rPosition, True);
		if (w->abacus.carryStep != 0) {
			(void) strncpy(buffer2, "Borrowing", 10);
		} else {
			if (bDigit != 0 && bDigit != ((root == 2) ? bValue : cValue))
				(void) sprintf(buffer2,
					"Subtract %d (part of %d) from primary field, offset digit by corresponding position %d (in group %d)%s",
					bDigit,
					((root == 2) ? bValue : cValue),
					rPosition, w->abacus.regCount,
					((w->abacus.carry[w->abacus.state] != 0) ? ", with borrow" : ""));
			else
				(void) sprintf(buffer2,
					"Subtract %d from primary field, offset digit by corresponding position %d (in group %d)%s",
					bDigit, rPosition, w->abacus.regCount,
					((w->abacus.carry[w->abacus.state] != 0) ? ", with borrow" : ""));
		}
		drawLineText(w, buffer2, 1);
		if (w->abacus.carryStep == 0) {
			w->abacus.step++;
		} else {
			w->abacus.carryStep++;
		}
	} else {
		aux = 0;
		if (!w->abacus.rightToLeftAdd)
			w->abacus.carry[1] = w->abacus.carry[0];
		w->abacus.state = 1;
		/*done = */(void) nextPositionPOField(w, &dPosition, &bDigit,
			(rStep % nSteps - 4) / 2);
		rPosition = decimalPlaceToRailPosition(w, aux, dPosition);
		setStringBuffer(w, w->abacus.rString, aux,
			dPosition, w->abacus.lower, w->abacus.upper);
		if (w->abacus.lower != 0)
			setAbacusMove(w, ACTION_MOVE, aux, 0, rPosition,
				w->abacus.lower);
		if (w->abacus.upper != 0)
			setAbacusMove(w, ACTION_MOVE, aux, 1, rPosition,
				w->abacus.upper);
		showHighlightRail(w, aux, rPosition, False);
		if (w->abacus.carry[w->abacus.state] == 0 &&
				w->abacus.carryStep != 0) {
			w->abacus.carryStep = 0;
		}
		if (rStep % nSteps != nSteps - 1 &&
				w->abacus.carry[w->abacus.state] == 0 &&
				logInt(((root == 2) ? bValue : cValue),
				w->abacus.base) <= (rStep % nSteps - 4) / 2) {
			w->abacus.step = (w->abacus.step / nSteps + 1) * nSteps + 2;
		} else {
			if (!w->abacus.rightToLeftAdd) {
				if (w->abacus.carry[w->abacus.state] != 0) {
					if (w->abacus.carryStep == 0) {
						w->abacus.carryStep = 2;
					} else {
						w->abacus.carryStep++;
					}
				}
			}
			if (w->abacus.rightToLeftAdd ||
						w->abacus.carryStep == 0) {
				w->abacus.step++;
			}
		}
	}
}
#endif

/* A little tricky as this is reentrant */
void teachStep(AbacusWidget w, char * buffer, int aux)
{
	char buffer1[120] = "";
	char buffer2[120] = "";
	char buffer3[120] = "";
	int base = w->abacus.base;
	int bottomNumber = w->abacus.decks[BOTTOM].number;
	double a, b;

#ifdef HAVE_GETTEXT
	setlocale(LC_ALL, "");
	/*bindtextdomain("AbacusT", getenv("PWD"));*/
	bindtextdomain("AbacusT", "/usr/share/locale/");
	textdomain("AbacusT");
#endif

#ifdef TEST
	if (aux == 0) {
#if 0
		testTeach(w, '+', 3, 8, 2, 3, 10);
		testTeach(w, '+', 3, 8, 2, 3, 10);
		testTeach(w, '+', 3, 8, 2, 3, 10);
		testTeach(w, 'v', 1.0, 0, 10102, 2, 3, 10000);
		testTeach(w, 'u', 1.0, 0, 1002, 2, 3, 10000);
		testTeachRoot(w, 3, 1002, 10000);
#endif
		testTeachRoot(w, 0, 102, 10000);
	} else {
		aux = 0;
	}
#endif
	if (w->abacus.step == 0) {
#ifndef TEST
		if (hOn == TRUE) {
			showHighlightRail(w, hAux, hRail, FALSE);
		}
#endif
#ifdef OLD_DEBUG
		testTable();
#endif
		drawLineText(w, (char *) TEACH_STRING0, 0);
		drawLineText(w, (char *) TEACH_STRING1, 1);
		(void) sprintf(buffer3, "(addition order will be: %s; multiplicand order will be %s)",
			((w->abacus.rightToLeftAdd) ? "right to left" : "left to right"),
			((w->abacus.rightToLeftMult) ? "right to left" : "left to right"));
		drawLineText(w, buffer3, 2);
		w->abacus.reg = -1;
		w->abacus.regCount = 0;
		w->abacus.carryStep = 0;
		w->abacus.carry[0] = 0;
		w->abacus.carry[1] = 0;
#ifndef WINVER
		if (w->abacus.aString)
			free(w->abacus.aString);
		if (w->abacus.bString)
			free(w->abacus.bString);
		if (w->abacus.cString)
			free(w->abacus.cString);
		if (w->abacus.rString)
			free(w->abacus.rString);
		if (w->abacus.sString)
			free(w->abacus.sString);
		if ((w->abacus.aString = (char *) malloc(sizeof (char) *
				w->abacus.numDigits + 2)) &&
			(w->abacus.bString = (char *) malloc(sizeof (char) *
				w->abacus.numDigits + 2)) &&
			(w->abacus.cString = (char *) malloc(sizeof (char) *
				w->abacus.numDigits + 2)) &&
			(w->abacus.rString = (char *) malloc(sizeof (char) *
				w->abacus.numDigits + 2)) &&
			(w->abacus.sString = (char *) malloc(sizeof (char) *
				w->abacus.numDigits + 2)))
#endif
			w->abacus.step++;
	} else if (w->abacus.step == 1) {
		simpleParser(w, buffer, w->abacus.aString, w->abacus.bString,
			&(w->abacus.op));
#ifdef DEBUG
		(void) printf("buffer = %s %c %s\n",
			w->abacus.aString, w->abacus.op, w->abacus.bString);
#endif
		if (w->abacus.op == 'q' || w->abacus.op == 'Q') {
			return;
		}
		if (w->abacus.anomaly != 0) {
			addBackAnomaly(w->abacus.aString, w->abacus.anomaly,
				w->abacus.shiftAnomaly,
				base);
		}
		if (w->abacus.anomalySq != 0) {
			addBackAnomaly(w->abacus.aString, w->abacus.anomalySq,
				w->abacus.shiftAnomaly + w->abacus.shiftAnomalySq,
				base);
		}
		if (checkSubdeck(w, 3)) {
			zeroFractionalPart(w->abacus.aString);
			zeroFractionalPart(w->abacus.bString);
		}
		contractStringBuffer(w->abacus.aString);
		contractStringBuffer(w->abacus.bString);
#ifdef OLD_DEBUG
		testShift(w->abacus.aString);
#endif
		if (w->abacus.op == '+' || w->abacus.op == '-') {
			convertStringToAbacus(w, w->abacus.aString, aux);
			if (w->abacus.leftAux != NULL)
				convertStringToAbacus(w, ZERO_STRING, 1);
			if (w->abacus.rightAux != NULL)
				convertStringToAbacus(w, ZERO_STRING, 2);
			a = convertToDecimal(base, w->abacus.aString);
			b = convertToDecimal(base, w->abacus.bString);
			if (w->abacus.op == '-' && b > a) {
				/* Revisit this, it should be allowed, but
				   # of rails probably need to be respected. */
				(void) sprintf(buffer1,
					"Subtraction underflow %s - %s",
					w->abacus.aString, w->abacus.bString);
				drawLineText(w, buffer1, 0);
				w->abacus.step = 0;
				return;
			}
			(void) sprintf(buffer1, "%sing %s %c %s",
				((w->abacus.op == '+') ? "Add" : "Subtract"),
				w->abacus.aString, w->abacus.op,
				w->abacus.bString);
			drawLineText(w, buffer1, 0);
			decimalSafe(w->abacus.aString);
			decimalSafe(w->abacus.bString);
			addSafe(w->abacus.aString, w->abacus.bString);
			(void) strcpy(w->abacus.rString, w->abacus.aString);
			(void) sprintf(buffer3, "Current answer: %s  ",
				w->abacus.rString);
			drawLineText(w, buffer3, 2);
			w->abacus.step++;
		} else if (w->abacus.op == '*') {
			if (strlen(w->abacus.aString) == 0)
				return;
			convertStringToAbacus(w, ZERO_STRING, 0);
			if (w->abacus.leftAux != NULL)
				convertStringToAbacus(w, w->abacus.aString, 1);
			a = convertToDecimal(base, w->abacus.aString);
			if (w->abacus.rightAux != NULL)
				convertStringToAbacus(w, w->abacus.bString, 2);
			b = convertToDecimal(base, w->abacus.bString);
			if (a < 0.0 || b < 0.0) {
				(void) sprintf(buffer1,
					"Multiplication underflow %s * %s",
					w->abacus.aString, w->abacus.bString);
				drawLineText(w, buffer1, 0);
				w->abacus.step = 0;
				return;
			}
			(void) sprintf(buffer1, "Multiplying %s %c %s",
				w->abacus.aString, w->abacus.op,
				w->abacus.bString);
#ifndef WINVER
			if (!w->abacus.lee) {
				(void) strncat(buffer1,
					" ... works best with lee option", 32);
			}
#endif
			drawLineText(w, buffer1, 0);
			decimalSafe(w->abacus.aString);
			decimalSafe(w->abacus.bString);
			(void) strncpy(w->abacus.rString, "0.", 3);
			(void) sprintf(buffer3, "Current answer: %s  ",
				w->abacus.rString);
			drawLineText(w, buffer3, 2);
			w->abacus.step++;
		} else if (w->abacus.op == '/') {
			if (strlen(w->abacus.aString) == 0)
				return;
			convertStringToAbacus(w, ZERO_STRING, 2);
			convertStringToAbacus(w, w->abacus.aString, 0);
			a = convertToDecimal(base, w->abacus.aString);
			if (w->abacus.rightAux != NULL)
				convertStringToAbacus(w, w->abacus.bString, 1);
			b = convertToDecimal(base, w->abacus.bString);
			if (a < 0.0 || b < 0.0) {
				(void) sprintf(buffer1,
					"Division underflow %s * %s",
					w->abacus.aString, w->abacus.bString);
				drawLineText(w, buffer1, 0);
				w->abacus.step = 0;
				return;
			}
			if (b == 0.0) {
				(void) sprintf(buffer1, "Division overflow %s / %s",
					w->abacus.aString, w->abacus.bString);
				drawLineText(w, buffer1, 0);
				w->abacus.step = 0;
				return;
			}
			contractStringBuffer(w->abacus.aString);
			(void) sprintf(buffer1, "Dividing %s %c %s",
				w->abacus.aString, w->abacus.op,
				w->abacus.bString);
#ifndef WINVER
			if (!w->abacus.lee) {
				(void) strncat(buffer1,
					" ... works best with lee option", 32);
			}
#endif
			drawLineText(w, buffer1, 0);
			decimalSafe(w->abacus.aString); /* quotient */
			decimalSafe(w->abacus.bString);
			(void) strcpy(w->abacus.rString, w->abacus.aString);
			(void) strncpy(w->abacus.sString, "0.", 3);
			(void) sprintf(buffer3, "Current answer: %s  ",
				w->abacus.sString);
			drawLineText(w, buffer3, 2);
			w->abacus.step++;
			w->abacus.reg = 0;
			(void) strcpy (w->abacus.cString, w->abacus.aString); /* remainder */
		} else if (w->abacus.op == 'v' || w->abacus.op == 'u') {
			char *group = (char *) malloc(sizeof (char) * (sizeof(w->abacus.intGroup) * 11));
			group[0] = '\0';
			rootGroup(w, w->abacus.aString,
				(w->abacus.op == 'v') ? 2 : 3,
				&(w->abacus.intGroup));
			stringGroup(w->abacus.intGroup, group, w->abacus.op);
			convertStringToAbacus(w, w->abacus.aString, aux);
			if (w->abacus.leftAux != NULL)
				convertStringToAbacus(w, ZERO_STRING, 1);
			if (w->abacus.rightAux != NULL)
				convertStringToAbacus(w, ZERO_STRING, 2);
			a = convertToDecimal(base, w->abacus.aString);
			(void) sprintf(buffer1, "%s root of %s, grouping digits yields (%s)",
				((w->abacus.op == 'v') ? "Square" : "Cube"),
				w->abacus.aString, group);
			free(group);
#ifndef WINVER
			if (!w->abacus.lee) {
				(void) strncat(buffer1,
					" ... works best with lee option", 32);
			}
#endif
			drawLineText(w, buffer1, 0);
			(void) strcpy(w->abacus.rString, w->abacus.aString);
			(void) strncpy(w->abacus.sString, "0.", 3);
			(void) sprintf(buffer3, "Current answer: %s  ",
				w->abacus.sString);
			drawLineText(w, buffer3, 2);
			w->abacus.step++;
			(void) strncpy(w->abacus.bString, "0.", 3);
			(void) strncpy(w->abacus.cString, "0.", 3);
#ifdef LEE_ROOT
			w->abacus.reg = -1;
#else
			w->abacus.reg = 0;
#endif
		}
#ifdef DEBUG
		(void) printf("op buffer: %s %c %s %d %d\n",
			w->abacus.aString, w->abacus.op,
			w->abacus.bString, w->abacus.rightToLeftAdd,
			w->abacus.rightToLeftMult);
#endif
		w->abacus.carry[0] = 0;
		w->abacus.carry[1] = 0;
#ifdef LEE_ROOT
	} else if (w->abacus.op == 'v' || w->abacus.op == 'u') {
		/* calculate separately due to complexity */
		teachStepRoot(w);
		return;
#endif
	} else if (w->abacus.reg == 0 || (w->abacus.reg < 0 &&
			((w->abacus.carryStep != 0 && w->abacus.carryStep % 2 == 0 &&
			w->abacus.carryStep >= 2) ||
			(w->abacus.carryStep == 0 && w->abacus.step % 2 == 0 &&
			w->abacus.step >= 2)))) {
		/* Tell user what is going to happen */
		int dPosition = 0;
		int aDigit = 0, bDigit = 0, bValue = 0;
		Boolean done = False;

		w->abacus.state = 0;
		if (w->abacus.op == '+' || w->abacus.op == '-') {
			done = nextPositionSum(w, w->abacus.op,
				&dPosition, &aDigit, &bDigit);
			(void) sprintf(buffer1, "%sing %s %c %s",
				((w->abacus.op == '+') ? "Add" : "Subtract"),
				w->abacus.aString, w->abacus.op,
				w->abacus.bString);
			if (w->abacus.carryStep == 0) {
				char buf[120];

				convertFromInteger(buf, base, aDigit);
				(void) strncat(buffer1, " ... ", 6);
				(void) strncat(buffer1, buf, 119);
				convertFromInteger(buf, base, bDigit);
				(void) strncat(buffer1, " ", 2);
				(void) strAddChar(buffer1, w->abacus.op);
				(void) strncat(buffer1, " ", 2);
				(void) strncat(buffer1, buf, 119);
				if (w->abacus.op == '+')
					convertFromInteger(buf, base, aDigit + bDigit);
				else
					convertFromInteger(buf, base, aDigit - bDigit);
				(void) strncat(buffer1, " = ", 4);
				(void) strncat(buffer1, buf, 119);
				convertFromInteger(buf, base, bValue);
			} else {
				(void) strncat(buffer1, " ... carrying 1", 16);
			}
			drawLineText(w, buffer1, 0);
		} else if (w->abacus.op == '*') {
			done = nextPositionProduct(w,
				&dPosition, &aDigit, &bDigit, &bValue);
			(void) sprintf(buffer1, "Multiplying %s %c %s",
				w->abacus.aString, w->abacus.op,
				w->abacus.bString);
			if (w->abacus.carryStep == 0) {
				char buf[120];

				convertFromInteger(buf, base, aDigit);
				(void) strncat(buffer1, " ... ", 6);
				(void) strncat(buffer1, buf, 119);
				convertFromInteger(buf, base, bDigit);
				(void) strncat(buffer1, " * ", 4);
				(void) strncat(buffer1, buf, 119);
				convertFromInteger(buf, base, aDigit * bDigit);
				(void) strncat(buffer1, " = ", 4);
				(void) strncat(buffer1, buf, 119);
				convertFromInteger(buf, base, bValue);
				(void) strncat(buffer1, ", adding ", 10);
				(void) strncat(buffer1, buf, 119);
				(void) strncat(buffer1, " digit", 7);
			} else {
				(void) strncat(buffer1, " ... carrying 1", 16);
			}
			drawLineText(w, buffer1, 0);
		} else if (w->abacus.op == '/') {
			done = nextPositionDivision(w,
				&dPosition, &aDigit, &bDigit, &bValue);
			contractStringBuffer(w->abacus.cString);
			(void) sprintf(buffer1, "Dividing %s %c %s",
				w->abacus.cString, w->abacus.op,
				w->abacus.bString);
			if (w->abacus.carryStep == 0 && w->abacus.divisor != 0) {
				char buf[120];

				convertFromInteger(buf, base, w->abacus.qDigit);
				(void) strncat(buffer1, " ... ", 6);
				(void) strncat(buffer1, buf, 119);
				convertFromDouble(buf, base, w->abacus.divisor);
				(void) strncat(buffer1, " * ", 4);
				(void) strncat(buffer1, buf, 119);
				convertFromDouble(buf, base,
					w->abacus.divisor * w->abacus.qDigit);
				(void) strncat(buffer1, " = ", 4);
				(void) strncat(buffer1, buf, 119);
				if (w->abacus.reg >= 0) {
					AbacusWidget *wap = &w, wa;
					int bn;
					int qrPosition;

					if (w->abacus.rightAux != NULL)
						wap = (AbacusWidget *) w->abacus.rightAux;
					wa = *wap;
					bn = wa->abacus.decks[BOTTOM].number;
					convertFromInteger(buf, base,
						w->abacus.qDigit);
					(void) strncat(buffer1, ", register ", 12);
					(void) strncat(buffer1, buf, 119);
					(void) strncat(buffer1, " on second auxiliary, rail ", 28);
					qrPosition = decimalPlaceToRailPosition(w, 2, w->abacus.qPosition);
					(void) strAddInt(buffer1, qrPosition);
					drawLineText(w, buffer1, 0);
					w->abacus.reg++;
					(void) pendingUpdate(w, buffer2, 1,
						2, qrPosition, base, bn);
					return;
				}
				if (w->abacus.divisor != bDigit) {
					convertFromInteger(buf, base, w->abacus.qDigit);
					(void) strncat(buffer1, " ... ", 6);
					(void) strncat(buffer1, buf, 119);
					convertFromInteger(buf, base, bDigit);
					(void) strncat(buffer1, " * ", 4);
					(void) strncat(buffer1, buf, 119);
					convertFromInteger(buf, base,
						w->abacus.qDigit * bDigit);
					(void) strncat(buffer1, " = ", 4);
					(void) strncat(buffer1, buf, 119);
				}
				convertFromInteger(buf, base, bValue);
				(void) strncat(buffer1, ", subtracting ", 15);
				(void) strncat(buffer1, buf, 119);
				(void) strncat(buffer1, " digit", 7);
				drawLineText(w, buffer1, 0);
			} else {
				(void) strncat(buffer1, " ... borrowing 1", 17);
			}
			drawLineText(w, buffer1, 0);
		} else if (w->abacus.op == 'v' || w->abacus.op == 'u') {
			done = nextPositionRoot(w,
				(w->abacus.op == 'v') ? 2 : 3,
				&dPosition, &aDigit, &bDigit, &bValue);
			(void) sprintf(buffer1, "Taking %s Root of %s",
				(w->abacus.op == 'v') ? "Square" : "Cube",
				w->abacus.cString);
			if (w->abacus.carryStep == 0) {
				(void) strncat(buffer1, " ... ", 6);
				(void) strAddInt(buffer1, w->abacus.qDigit);
				if (w->abacus.op == 'u') {
					(void) strncat(buffer1, " * ", 4);
					(void) strAddInt(buffer1, w->abacus.qDigit);
				}
				(void) strncat(buffer1, " * ", 4);
				(void) strAddInt(buffer1, w->abacus.qDigit);
				(void) strncat(buffer1, " = ", 4);
				(void) strAddInt(buffer1, (w->abacus.qDigit * w->abacus.qDigit *
					((w->abacus.op == 'v') ? 1 : w->abacus.qDigit)));
				if (w->abacus.reg >= 0) {
					AbacusWidget *wap = &w, wa;
					int bn;

					if (w->abacus.leftAux != NULL)
						wap = (AbacusWidget *) w->abacus.leftAux;
					wa = *wap;
					bn = wa->abacus.decks[BOTTOM].number;
					(void) strncat(buffer1, ", register ", 12);
					(void) strAddInt(buffer1, w->abacus.qDigit);
					(void) strncat(buffer1, " on first auxiliary, rail ", 27);
					(void) strAddInt(buffer1, w->abacus.qPosition);
					drawLineText(w, buffer1, 0);
					w->abacus.reg++;
					(void) pendingUpdate(w, buffer2, 1,
						1, w->abacus.qPosition, base, bn);
					return;
				}
				(void) strncat(buffer1, " ... subtracting ", 18);
				(void) strAddInt(buffer1, bValue);
				(void) strncat(buffer1, " digit", 7);
				drawLineText(w, buffer1, 0);
			} else {
				(void) strncat(buffer1, " ... borrowing 1", 17);
			}
			drawLineText(w, buffer1, 0);
		}
		if (!pendingUpdate(w, buffer2, 1, 0,
				decimalPlaceToRailPosition(w, 0, dPosition),
				base, bottomNumber))
			done = False;
		if (w->abacus.carry[w->abacus.state] == 0 &&
				w->abacus.carryStep == 0 && done) {
			contractStringBuffer(w->abacus.rString);
			if (w->abacus.op == 'v' || w->abacus.op == 'u') {
				contractStringBuffer(w->abacus.sString);
				(void) sprintf(buffer3,
					"%s %s",
					_("Final answer:"),
					w->abacus.sString);
				showHighlightRails(w, 1);
			} else if (w->abacus.op == '/') {
				contractStringBuffer(w->abacus.sString);
				(void) sprintf(buffer3,
					"%s %s",
					_("Final answer:"),
					w->abacus.sString);
				showHighlightRails(w, 2);
			} else {
				(void) sprintf(buffer3,
					"%s %s",
					_("Final answer:"),
					w->abacus.rString);
				showHighlightRails(w, 0);
			}
			drawLineText(w, buffer3, 2);
			w->abacus.step = 0;
		} else if (w->abacus.carryStep == 0) {
			w->abacus.step++;
		} else {
			w->abacus.carryStep++;
		}
	} else {
		/* Actually carry out what was told would happen */
		int dPosition = 0, rPosition = 0;
		int aDigit = 0, bDigit = 0, bValue = 0;
		Boolean done = False;

		if (!w->abacus.rightToLeftAdd)
			w->abacus.carry[1] = w->abacus.carry[0];
		w->abacus.state = 1;
		if (w->abacus.op == '+' || w->abacus.op == '-') {
			done = nextPositionSum(w, w->abacus.op,
				&dPosition, &aDigit, &bDigit);
			rPosition = decimalPlaceToRailPosition(w, 0, dPosition);
			setStringBuffer(w, w->abacus.rString, 0,
				dPosition, w->abacus.lower, w->abacus.upper);
		} else if (w->abacus.op == '*') {
			done = nextPositionProduct(w,
				&dPosition, &aDigit, &bDigit, &bValue);
			rPosition = decimalPlaceToRailPosition(w, 0, dPosition);
			setStringBuffer(w, w->abacus.rString, 0,
				dPosition, w->abacus.lower, w->abacus.upper);
		} else if (w->abacus.op == '/') {
			done = nextPositionDivision(w,
				&dPosition, &aDigit, &bDigit, &bValue);
			rPosition = decimalPlaceToRailPosition(w, 0, dPosition);
			if (w->abacus.reg > 0) {
				int qrPosition = decimalPlaceToRailPosition(w, 2, w->abacus.qPosition);
				if (w->abacus.rightAux != NULL) {
					if (w->abacus.lower != 0)
						setAbacusMove(w, ACTION_MOVE, 2, 0, qrPosition,
							w->abacus.lower);
					if (w->abacus.upper != 0)
						setAbacusMove(w, ACTION_MOVE, 2, 1, qrPosition,
							w->abacus.upper);
					showHighlightRail(w, 2, qrPosition, False);
				}
				setStringBuffer(w, w->abacus.sString, 2,
					w->abacus.qPosition,
					w->abacus.lower, w->abacus.upper);
				w->abacus.reg = -1;
				contractStringBuffer(w->abacus.sString);
				(void) sprintf(buffer3, "Current answer: %s  ",
					w->abacus.sString);
				drawLineText(w, buffer3, 2);
				return;
			}
			setStringBuffer(w, w->abacus.rString, 0,
				dPosition, w->abacus.lower, w->abacus.upper);
			a = convertToDecimal(base, w->abacus.rString);
			if (a == 0.0) {
				done = True;
			}
		} else if (w->abacus.op == 'v' || w->abacus.op == 'u') {
			done = nextPositionRoot(w,
				(w->abacus.op == 'v') ? 2 : 3,
				&dPosition, &aDigit, &bDigit, &bValue);
			rPosition = decimalPlaceToRailPosition(w, 0, dPosition);
			if (w->abacus.reg > 0) {
				if (w->abacus.leftAux != NULL) {
					if (w->abacus.lower != 0)
						setAbacusMove(w, ACTION_MOVE, 1, 0, w->abacus.qPosition,
							w->abacus.lower);
					if (w->abacus.upper != 0)
						setAbacusMove(w, ACTION_MOVE, 1, 1, w->abacus.qPosition,
							w->abacus.upper);
					showHighlightRail(w, 1, w->abacus.qPosition, False);
				}
				setStringBuffer(w, w->abacus.sString, 1,
					w->abacus.qPosition,
					w->abacus.lower, w->abacus.upper);
				w->abacus.reg = -1;
				contractStringBuffer(w->abacus.sString);
				(void) sprintf(buffer3, "Current answer: %s  ",
					w->abacus.sString);
				drawLineText(w, buffer3, 2);
				return;
			}
			setStringBuffer(w, w->abacus.rString, 0,
				dPosition, w->abacus.lower, w->abacus.upper);
			a = convertToDecimal(base, w->abacus.rString);
			if (a == 0.0) {
				done = True;
			}
		}
		if (w->abacus.lower != 0)
			setAbacusMove(w, ACTION_MOVE, 0, 0, rPosition,
				w->abacus.lower);
		if (w->abacus.upper != 0)
			setAbacusMove(w, ACTION_MOVE, 0, 1, rPosition,
				w->abacus.upper);
		showHighlightRail(w, 0, rPosition, False);
#if 0
		if (w->abacus.carry[w->abacus.state] != 0) {
			setAbacusMove(w, ACTION_MOVE, 0, 0, rPosition + 1,
				w->abacus.carry[w->abacus.state]);
			w->abacus.carry[w->abacus.state] = 0;
		}
#endif
		if (w->abacus.carry[w->abacus.state] == 0 &&
				w->abacus.carryStep != 0) {
			w->abacus.carryStep = 0;
		}
		contractStringBuffer(w->abacus.rString);
		if (w->abacus.carry[w->abacus.state] == 0 &&
				w->abacus.carryStep == 0 &&
				done) {
			w->abacus.step = 0;
			if (w->abacus.op == 'v' || w->abacus.op == 'u') {
				contractStringBuffer(w->abacus.sString);
				(void) sprintf(buffer3,
					"%s %s",
					_("Final answer:"),
					w->abacus.sString);
				showHighlightRails(w, 1);
			} else if (w->abacus.op == '/') {
				contractStringBuffer(w->abacus.sString);
				(void) sprintf(buffer3,
					"%s %s",
					_("Final answer:"),
					w->abacus.sString);
				showHighlightRails(w, 2);
			} else {
				(void) sprintf(buffer3,
					"%s %s",
					_("Final answer:"),
					w->abacus.rString);
				showHighlightRails(w, 0);
			}
			drawLineText(w, buffer3, 2);
		} else {
			if (w->abacus.op == '/' ||
					w->abacus.op == 'v' || w->abacus.op == 'u') {
				contractStringBuffer(w->abacus.sString);
				(void) sprintf(buffer3, "Current answer: %s  ",
					w->abacus.sString);
			} else {
				(void) sprintf(buffer3, "Current answer: %s  ",
					w->abacus.rString);
			}
			drawLineText(w, buffer3, 2);
			if ((done && w->abacus.rightToLeftAdd) ||
					!w->abacus.rightToLeftAdd) {
				if (w->abacus.carry[w->abacus.state] != 0) {
					if (w->abacus.carryStep == 0) {
						w->abacus.carryStep = 2;
						if (w->abacus.rightToLeftAdd)
							w->abacus.carry[1] = w->abacus.carry[0] = 0;
					} else {
						w->abacus.carryStep++;
					}
				}
				if (w->abacus.carryStep == 0) {
					w->abacus.step++;
				}
			} else if (w->abacus.rightToLeftAdd) {
				w->abacus.step++;
			}
		}
	}
}

#ifdef TEST
void testTeachRoot(AbacusWidget w, int startX, int finishX, int max)
{
	char buffer[120] = "";
	int i, x, side/*, moves*/;
	double ans, rem, extra, diff, diff2, temp;
	char op;
	double factor, epsilon, epsilon2;
	int root, places;

	w->abacus.delay = 0;
	for (x = startX; x < finishX; x++) {
	  for (root = 2; root <= 3; root++) {
	    op = (root == 2) ? 'v' : 'u';
	    for (places = 0; places <= 1; places++) {
	      factor = expFloat(places, w->abacus.base);
	      epsilon = expFloat(root * places + 1, w->abacus.base);
	      epsilon2 = expFloat((root - 1) * places + 1, w->abacus.base);
	      for (side = 0; side <= 1; side++) {
	      		w->abacus.rightToLeftAdd = (side != 0);
			temp = 1.0;
			if (root == 3)
				temp *= factor * x;
			(void) sprintf(buffer, "%1.*f%c", places * root,
				factor * factor * x * x * temp, op);
			(void) printf("%1.*f: %s = ", places, factor * x,
				buffer);
			/*moves = 0;*/
			for (i = 0; i < max; i++) {
				teachStep(w, buffer, -1);
				if (w->abacus.step == 0) {
					break;
				}/* else {
					moves = w->abacus.step;
				}*/
			}
			trimStringBuffer(w->abacus.rString);
			trimStringBuffer(w->abacus.bString);
			if (root == 3)
				trimStringBuffer(w->abacus.cString);
			ans = atof(w->abacus.bString);
			rem = atof(w->abacus.rString);
			extra = atof(w->abacus.cString);
			diff = ABS(x * root * factor - ans);
			diff2 = 0;
			if (root == 3)
				diff2 = ABS(x * x * factor * factor * root - extra);
			(void) printf("%s / %d (remainder %s)%s%s%s, (%s) %s%s%s%s\n",
#ifdef LEE_ROOT
				w->abacus.bString,
#else
				w->abacus.sString,
#endif
				root, w->abacus.rString,
				((root == 3) ? ", (order " : ""),
				((root == 3) ? w->abacus.cString : ""),
				((root == 3) ? ")" : ""),
	   			((w->abacus.rightToLeftAdd) ? "r-l" : "l-r"),
				((w->abacus.step != 0) ? "s" : ""),
				((rem != 0.0) ? "r" : ""),
				((diff >= epsilon) ? "a" : ""),
				((diff2 >= epsilon2) ? "e" : ""));
			if ((w->abacus.step != 0) ||
					(rem != 0.0) ||
					(diff >= epsilon) ||
					(diff2 >= epsilon2)) {
				(void) printf("Error!\n");
				exit(-1);
			}
#if 0
			for (y = startY; y < finishY; y++) {
				(void) sprintf(buffer, "%d%c%d", x, op, y);
				(void) printf("%s = ", buffer);
				for (i = 0; i < max; i++) {
					teachStep(w, buffer, -1);
					if (w->abacus.step == 0)
						break;
				}
				if (op =='/') {
					(void) printf("%s\n", w->abacus.sString);
				} else {
					(void) printf("%s\n", w->abacus.rString);
				}
			}
#endif
	      }
	    }
	  }
	}
}
#endif
