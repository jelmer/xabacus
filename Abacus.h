/*
 * @(#)Abacus.h
 *
 * Copyright 1994 - 2017  David A. Bagley, bagleyd AT verizon.net
 *
 * Abacus demo and neat pointers from
 * Copyright 1991 - 1998  Luis Fernandes, elf AT ee.ryerson.ca
 *
 * All rights reserved.
 *
 * Permission to use, copy, modify, and distribute this software and
 * its documentation for any purpose and without fee is hereby granted,
 * provided that the above copyright notice appear in all copies and
 * that both that copyright notice and this permission notice appear in
 * supporting documentation, and that the name of the author not be
 * used in advertising or publicity pertaining to distribution of the
 * software without specific, written prior permission.
 *
 * This program is distributed in the hope that it will be "useful",
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

/* Public header file for Abacus */

#ifndef _XtAbacus_h
#define _XtAbacus_h

/***********************************************************************
 *
 * Abacus Widget
 *
 ***********************************************************************/

#if 0
#define DEBUG 1
#endif

#ifndef WINVER
#define XtNmono ((char *) "mono")
#define XtNframeColor ((char *) "frameColor")
#define XtNprimaryBeadColor ((char *) "primaryBeadColor")
#define XtNleftAuxBeadColor ((char *) "leftAuxBeadColor")
#define XtNrightAuxBeadColor ((char *) "rightAuxBeadColor")
#define XtNsecondaryBeadColor ((char *) "secondaryBeadColor")
#define XtNhighlightBeadColor ((char *) "highlightBeadColor")
#define XtNcheckerBeadColor ((char *) "checkerBeadColor")
#define XtNprimaryRailColor ((char *) "primaryRailColor")
#define XtNsecondaryRailColor ((char *) "secondaryRailColor")
#define XtNhighlightRailColor ((char *) "highlightRailColor")
#define XtNlineRailColor ((char *) "lineRailColor")
#define XtNbumpSound ((char *) "bumpSound")
#define XtNmoveSound ((char *) "moveSound")
#define XtNdripSound ((char *) "dripSound")
#define XtNsound ((char *) "sound")
#define XtNdelay ((char *) "delay")
#define XtNscript ((char *) "script")
#define XtNbuffer ((char *) "buffer")
#define XtNdemo ((char *) "demo")
#define XtNdemoPath ((char *) "demoPath")
#define XtNdemoFont ((char *) "demoFont")
#define XtNdemoForeground ((char *) "demoForeground")
#define XtNdemoBackground ((char *) "demoBackground")
#define XtNteach ((char *) "teach")
#define XtNteachBuffer ((char *) "teachBuffer")
#define XtNrightToLeftAdd ((char *) "rightToLeftAdd")
#define XtNrightToLeftMult ((char *) "rightToLeftMult")
#define XtNlee ((char *) "lee")
#define XtNrails ((char *) "rails")
#define XtNleftAuxRails ((char *) "leftAuxRails")
#define XtNrightAuxRails ((char *) "rightAuxRails")
#define XtNvertical ((char *) "vertical")
#define XtNcolorScheme ((char *) "colorScheme")
#define XtNslot ((char *) "slot")
#define XtNdiamond ((char *) "diamond")
#define XtNrailIndex ((char *) "railIndex")
#define XtNtopOrient ((char *) "topOrient")
#define XtNbottomOrient ((char *) "bottomOrient")
#define XtNtopNumber ((char *) "topNumber")
#define XtNbottomNumber ((char *) "bottomNumber")
#define XtNtopFactor ((char *) "topFactor")
#define XtNbottomFactor ((char *) "bottomFactor")
#define XtNtopSpaces ((char *) "topSpaces")
#define XtNbottomSpaces ((char *) "bottomSpaces")
#define XtNtopPiece ((char *) "topPiece")
#define XtNbottomPiece ((char *) "bottomPiece")
#define XtNtopPiecePercent ((char *) "topPiecePercent")
#define XtNbottomPiecePercent ((char *) "bottomPiecePercent")
#define XtNshiftPercent ((char *) "shiftPercent")
#define XtNsubdeck ((char *) "subdeck")
#define XtNsubbead ((char *) "subbead")
#define XtNsign ((char *) "sign")
#define XtNdecimalPosition ((char *) "decimalPosition")
#define XtNgroup ((char *) "group")
#define XtNgroupSize ((char *) "groupSize")
#define XtNdecimalComma ((char *) "decimalComma")
#define XtNbase ((char *) "base")
#define XtNsubbase ((char *) "subbase")
#define XtNanomaly ((char *) "anomaly")
#define XtNshiftAnomaly ((char *) "shiftAnomaly")
#define XtNanomalySq ((char *) "anomalySq")
#define XtNshiftAnomalySq ((char *) "shiftAnomalySq")
#define XtNdisplayBase ((char *) "displayBase")
#define XtNpressOffset ((char *) "pressOffset")
#define XtNromanNumerals ((char *) "romanNumerals")
#define XtNlatin ((char *) "latin")
#define XtNancientRoman ((char *) "ancientRoman")
#define XtNmodernRoman ((char *) "modernRoman")
#define XtNmode ((char *) "mode")
#define XtNformat ((char *) "format")
#define XtNsubmode ((char *) "submode")
#define XtNmuseum ((char *) "museum")
#define XtNversionOnly ((char *) "versionOnly")
#define XtNmenu ((char *) "menu")
#define XtNdeck ((char *) "deck")
#define XtNrail ((char *) "rail")
#define XtNnumber ((char *) "number")
#define XtNframed ((char *) "framed")
#define XtNaux ((char *) "aux")
#define XtNmathBuffer ((char *) "mathBuffer")
#define XtNpixmapSize ((char *) "pixmapSize")
#define XtNselectCallback ((char *) "selectCallback")
#define XtNleftAuxAbacus ((char *) "leftAuxAbacus")
#define XtNrightAuxAbacus ((char *) "rightAuxAbacus")
#define XtCMono ((char *) "Mono")
#define XtCFrameColor ((char *) "FrameColor")
#define XtCPrimaryBeadColor ((char *) "PrimaryBeadColor")
#define XtCLeftAuxBeadColor ((char *) "LeftAuxBeadColor")
#define XtCRightAuxBeadColor ((char *) "RightAuxBeadColor")
#define XtCSecondaryBeadColor ((char *) "SecondaryBeadColor")
#define XtCHighlightBeadColor ((char *) "HighlightBeadColor")
#define XtCPrimaryRailColor ((char *) "PrimaryRailColor")
#define XtCSecondaryRailColor ((char *) "SecondaryRailColor")
#define XtCHighlightRailColor ((char *) "HighlightRailColor")
#define XtCBumpSound ((char *) "BumpSound")
#define XtCMoveSound ((char *) "MoveSound")
#define XtCDripSound ((char *) "DripSound")
#define XtCSound ((char *) "Sound")
#define XtCDelay ((char *) "Delay")
#define XtCScript ((char *) "Script")
#define XtCBuffer ((char *) "Buffer")
#define XtCDemo ((char *) "Demo")
#define XtCDemoPath ((char *) "DemoPath")
#define XtCDemoFont ((char *) "DemoFont")
#define XtCDemoForeground ((char *) "DemoForeground")
#define XtCDemoBackground ((char *) "DemoBackground")
#define XtCTeach ((char *) "Teach")
#define XtCTeachBuffer ((char *) "TeachBuffer")
#define XtCRightToLeftAdd ((char *) "RightToLeftAdd")
#define XtCRightToLeftMult ((char *) "RightToLeftMult")
#define XtCLee ((char *) "Lee")
#define XtCRails ((char *) "Rails")
#define XtCLeftAuxRails ((char *) "LeftAuxRails")
#define XtCRightAuxRails ((char *) "RightAuxRails")
#define XtCVertical ((char *) "Vertical")
#define XtCColorScheme ((char *) "ColorScheme")
#define XtCSlot ((char *) "Slot")
#define XtCDiamond ((char *) "Diamond")
#define XtCRailIndex ((char *) "RailIndex")
#define XtCTopOrient ((char *) "TopOrient")
#define XtCBottomOrient ((char *) "BottomOrient")
#define XtCTopNumber ((char *) "TopNumber")
#define XtCBottomNumber ((char *) "BottomNumber")
#define XtCTopFactor ((char *) "TopFactor")
#define XtCBottomFactor ((char *) "BottomFactor")
#define XtCTopSpaces ((char *) "TopSpaces")
#define XtCBottomSpaces ((char *) "BottomSpaces")
#define XtCTopPiece ((char *) "TopPiece")
#define XtCBottomPiece ((char *) "BottomPiece")
#define XtCTopPiecePercent ((char *) "TopPiecePercent")
#define XtCBottomPiecePercent ((char *) "BottomPiecePercent")
#define XtCShiftPercent ((char *) "ShiftPercent")
#define XtCSubdeck ((char *) "Subdeck")
#define XtCSubbead ((char *) "Subbead")
#define XtCSign ((char *) "Sign")
#define XtCDecimalPosition ((char *) "DecimalPosition")
#define XtCGroup ((char *) "Group")
#define XtCGroupSize ((char *) "GroupSize")
#define XtCDecimalComma ((char *) "DecimalComma")
#define XtCBase ((char *) "Base")
#define XtCSubbase ((char *) "Subbase")
#define XtCAnomaly ((char *) "Anomaly")
#define XtCShiftAnomaly ((char *) "ShiftAnomaly")
#define XtCAnomalySq ((char *) "AnomalySq")
#define XtCShiftAnomalySq ((char *) "ShiftAnomalySq")
#define XtCDisplayBase ((char *) "DisplayBase")
#define XtCPressOffset ((char *) "PressOffset")
#define XtCRomanNumerals ((char *) "RomanNumerals")
#define XtCLatin ((char *) "Latin")
#define XtCAncientRoman ((char *) "AncientRoman")
#define XtCModernRoman ((char *) "ModernRoman")
#define XtCMode ((char *) "Mode")
#define XtCFormat ((char *) "Format")
#define XtCSubmode ((char *) "Submode")
#define XtCMuseum ((char *) "Museum")
#define XtCMenu ((char *) "Menu")
#define XtCDeck ((char *) "Deck")
#define XtCRail ((char *) "Rail")
#define XtCNumber ((char *) "Number")
#define XtCFramed ((char *) "Framed")
#define XtCAux ((char *) "Aux")
#define XtCMathBuffer ((char *) "MathBuffer")
#define XtCPixmapSize ((char *) "PixmapSize")
#define XtCLeftAuxAbacus ((char *) "LeftAuxAbacus")
#define XtCRightAuxAbacus ((char *) "RightAuxAbacus")
typedef struct _AbacusClassRec *AbacusWidgetClass;

extern WidgetClass abacusWidgetClass;
extern WidgetClass abacusDemoWidgetClass;

typedef struct {
	XEvent     *event;
	int         reason;
	char       *buffer, *mathBuffer;
	char       *teachBuffer;
	int         aux, deck, rail, number;
	int         line;
} abacusCallbackStruct;
#endif

#define ACTION_EXIT 100
#define ACTION_HIDE 101
#define ACTION_BASE_DEFAULT 102
#define ACTION_DEMO_DEFAULT 103
#define ACTION_CLEAR_QUERY 104
#define ACTION_CALC 105
#define ACTION_SCRIPT 106
#define ACTION_MOVE 107
#define ACTION_PLACE 108
#define ACTION_HIGHLIGHT_RAIL 109
#define ACTION_UNHIGHLIGHT_RAIL 110
#define ACTION_HIGHLIGHT_RAILS 111
#define ACTION_CLEAR 200
#define ACTION_DECIMAL_CLEAR 201
#define ACTION_COMPLEMENT 202
#define ACTION_INCREMENT 210
#define ACTION_DECREMENT 211
#define ACTION_FORMAT 220
#define ACTION_ROMAN_NUMERALS 230
#define ACTION_ANCIENT_ROMAN 231
#define ACTION_LATIN 232
#define ACTION_GROUP 233
#define ACTION_SIGN 240
#define ACTION_QUARTER 241
#define ACTION_QUARTER_PERCENT 242
#define ACTION_TWELFTH 243
#define ACTION_SUBDECK 250
#define ACTION_EIGHTH 251
#define ACTION_MUSEUM 252
#define ACTION_MODERN_ROMAN 260
#define ACTION_ANOMALY 261
#define ACTION_WATCH 262
#define ACTION_VERTICAL 263
#define ACTION_TEACH 264
#define ACTION_RIGHT_TO_LEFT_ADD 270
#define ACTION_RIGHT_TO_LEFT_MULT 271
#define ACTION_SOUND 272
#define ACTION_SPEED_UP 273
#define ACTION_SLOW_DOWN 274
#define ACTION_CLEAR_NODEMO 275
#define ACTION_TEACH_LINE 276
#define ACTION_DEMO 300
#define ACTION_NEXT 301
#define ACTION_REPEAT 302
#define ACTION_JUMP 303
#define ACTION_MORE 304
#define ACTION_CHAPTER1 311
#define ACTION_CHAPTER2 312
#define ACTION_CHAPTER3 313
#define ACTION_CHAPTER4 314
#define ACTION_CHAPTER5 315
#define ACTION_CHAPTER6 316
#define ACTION_CHAPTER7 317
#define ACTION_CHAPTER8 318
#define ACTION_DEMO1 331
#define ACTION_DEMO2 332
#define ACTION_DEMO3 333
#define ACTION_DEMO4 334
#define ACTION_TEACH1 351
#define ACTION_TEACH2 352
#define ACTION_TEACH3 353
#define ACTION_RIGHT 401
#define ACTION_LEFT 403
#define ACTION_UP 400
#define ACTION_DOWN 402
#define ACTION_INCX 501
#define ACTION_DECX 503
#define ACTION_INCY 500
#define ACTION_DECY 502
#define ACTION_DESCRIPTION 900
#define ACTION_FEATURES 901
#define ACTION_REFERENCES 902
#define ACTION_ABOUT 903
#define ACTION_IGNORE 999

#define HIGHLIGHTS_DECK (-12)
#define UNHIGHLIGHT_DECK (-11)
#define HIGHLIGHT_DECK (-10)
#define TEACH_DECK (-9)
#define CALC_DECK (-8)
#define MORE_DECK (-7)
#define JUMP_DECK (-6)
#define REPEAT_DECK (-5)
#define NEXT_DECK (-4)
#define CLEAR_DECIMAL_DECK (-3)
#define CLEAR_DECK (-2)
#define IGNORE_DECK (-1)
#define MIN_RAILS 1
#define MIN_DEMO_RAILS 3
#define DEFAULT_RAILS 13
#define DEFAULT_TOP_SPACES 2
#define DEFAULT_BOTTOM_SPACES 2
#define DEFAULT_TOP_NUMBER 2
#define DEFAULT_BOTTOM_NUMBER 5
#define DEFAULT_TOP_FACTOR 5
#define DEFAULT_BOTTOM_FACTOR 1
#define DEFAULT_TOP_ORIENT TRUE
#define DEFAULT_BOTTOM_ORIENT FALSE
#define MIN_BASE 2 /* Base 1 is rediculous :) */
#define MAX_BASE 36 /* 10 numbers + 26 letters (ASCII) */
#define DEFAULT_BASE 10
#define DEFAULT_SUBDECKS 3
#define DEFAULT_SUBBEADS 4
#define DEFAULT_DECIMAL_POSITION 2
#define DEFAULT_SHIFT_PERCENT 2
#define DEFAULT_SHIFT_ANOMALY 2
#define DEFAULT_GROUP_SIZE 3
#define SUBDECK_SPACE 1
#define MAX_MUSEUMS 3
#define IT 0
#define UK 1
#define FR 2
#define COLOR_MIDDLE 1
#define COLOR_FIRST 2
#define COLOR_HALF 4
#define PRIMARY 0
#define LEFT_AUX 1
#define RIGHT_AUX 2
#ifdef MONOTEST
#define DEFAULT_MONO TRUE
#else
#define DEFAULT_MONO FALSE
#endif
#define DEFAULT_REVERSE FALSE
#define CHINESE 0
#define JAPANESE 1
#define KOREAN 2
#define RUSSIAN 3
#define DANISH 4
#define ROMAN 5
#define MEDIEVAL 6
#define GENERIC 7
#define MAX_FORMATS 7
#define MAX_MODES 8
#define TEACH_STRING0 "Enter calculation X+Y, X-Y, X*Y, X/Y, Xv, or Xu where X and result nonnegative."
#define TEACH_STRING1 "Press enter to go through calculation steps."
#define ZERO_STRING "0.0"

typedef struct _AbacusRec *AbacusWidget;
typedef struct _AbacusRec *AbacusDemoWidget;

#endif /* _XtAbacus_h */
/* DON'T ADD STUFF AFTER THIS #endif */
