/*
 * @(#)AbacusE.c
 *
 * Copyright 2017 - 2018  David A. Bagley, bagleyd AT verizon.net
 *
 * Abacus examples
 * All rights reserved.
 *
 * Permission to use, copy, modify, and distribute this software and
 * its documentation for any purpose and without fee is hereby granted,
 * provided that the above copyright notice appear in all copies and
 * that both that copyright notice and this permission notice appear in
 * supporting documentation, and that the name of the author not be
 * used in advertising or publicity pertaining to distribution of the
 * software without specific, written prior permission.
 *
 * This program is distributed in the hope that it will be "useful",
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

/* Method to handle the examples for Abacus */

/**
 * Much help from: Dodji Seketeli
 * http://www.xmlsoft.org/examples/tree1.c
 */

#include "AbacusP.h"

static Boolean debug = False;

unsigned int codeMaxCount = 0; /*4*/
unsigned int moveMaxCount = 0; /*41*/
unsigned int lessonMaxCount = 0; /*7*/
unsigned int chapterMaxCount = 0; /*7*/
unsigned int editionMaxCount = 0; /*4*/
unsigned int bookMaxCount = 0; /*3*/

static void
freeMove(struct moveType *moveAbacus) {
	unsigned int i;

	if (moveAbacus == NULL)
		return;
	if (moveAbacus->code != NULL) {
		free(moveAbacus->code);
		moveAbacus->code = NULL;
	}
	if (moveAbacus->lineText != NULL) {
		for (i = 0; i < codeMaxCount; i++) {
			if (moveAbacus->lineText[i] != NULL)
				free(moveAbacus->lineText[i]);
		}
		free(moveAbacus->lineText);
		moveAbacus->lineText = NULL;
	}
}

static void
freeLesson(struct lessonType *lessonAbacus) {
	unsigned int move;

	if (lessonAbacus == NULL)
		return;
	if (lessonAbacus->move != NULL) {
		for (move = 0; move < moveMaxCount; move++) {
			freeMove(&lessonAbacus->move[move]);
		}
		free(lessonAbacus->move);
		lessonAbacus->move = NULL;
	}
	if (lessonAbacus->name != NULL) {
		free(lessonAbacus->name);
		lessonAbacus->name = NULL;
	}
	if (debug) {
		(void) printf("\t\t\t\tlesson free %u/%u moves\n",
			lessonAbacus->moves, moveMaxCount);
	}
	lessonAbacus->moves = 0;
}

static void
freeChapter(struct chapterType *chapterAbacus) {
	unsigned int lesson;

	if (chapterAbacus == NULL)
		return;
	if (chapterAbacus->lesson != NULL) {
		for (lesson = 0; lesson < lessonMaxCount; lesson++) {
			freeLesson(&chapterAbacus->lesson[lesson]);
		}
		free(chapterAbacus->lesson);
		chapterAbacus->lesson = NULL;
	}
	if (chapterAbacus->name != NULL) {
		free(chapterAbacus->name);
		chapterAbacus->name = NULL;
	}
	if (debug) {
		(void) printf("\t\t\tchapter free %u/%u lessons\n",
			chapterAbacus->lessons, lessonMaxCount);
	}
	chapterAbacus->lessons = 0;
}

static void
freeEdition(struct editionType *editionAbacus) {
	unsigned int chapter;

	if (editionAbacus == NULL)
		return;
	if (editionAbacus->chapter != NULL) {
		for (chapter = 0; chapter < chapterMaxCount; chapter++) {
			freeChapter(&editionAbacus->chapter[chapter]);
		}
		free(editionAbacus->chapter);
		editionAbacus->chapter = NULL;
	}
	if (editionAbacus->version != NULL) {
		free(editionAbacus->version);
		editionAbacus->version = NULL;
	}
	if (debug) {
		(void) printf("\t\tedition free %u/%u chapters\n",
			editionAbacus->chapters, chapterMaxCount);
	}
	editionAbacus->chapters = 0;
}

static void
freeBook(struct bookType *bookAbacus) {
	unsigned int edition;

	if (bookAbacus == NULL)
		return;
	if (bookAbacus->edition != NULL) {
		for (edition = 0; edition < editionMaxCount; edition++) {
			freeEdition(&bookAbacus->edition[edition]);
		}
		free(bookAbacus->edition);
		bookAbacus->edition = NULL;
	}
	if (bookAbacus->name != NULL) {
		free(bookAbacus->name);
		bookAbacus->name = NULL;
	}
	if (bookAbacus->author != NULL) {
		free(bookAbacus->author);
		bookAbacus->author = NULL;
	}
	if (debug) {
		(void) printf("\tbook free %u/%u editions\n",
			bookAbacus->editions, editionMaxCount);
	}
	bookAbacus->editions = 0;
}

void
freeAbacus(struct bookType *bookAbacus, unsigned int books) {
	unsigned int book;

	if (bookAbacus == NULL)
		return;
	for (book = 0; book < books; book++) {
		freeBook(&bookAbacus[book]);
	}
	free(bookAbacus);
	bookAbacus = NULL;
	if (debug) {
		(void) printf("abacus free %u/%u books\n",
			books, bookMaxCount);
	}
}

/* This can probably be fixed to use libxml2 Windows port */
/* https://www.zlatkovic.com/libxml.en.html */
#ifdef HAVE_LIBXML2
#include <libxml/parser.h>
#include <libxml/tree.h>
#endif

#ifdef LIBXML_TREE_ENABLED

xmlDoc *doc = NULL;

/* From AndiDog
 https://stackoverflow.com/questions/2450704/writing-string-trim-in-c */
static void
trim(char *str)
{
	char *ptr = str;
	char *end;

	while (*ptr == ' ' || *ptr == '\t' || *ptr == '\r' || *ptr == '\n')
		++ptr;

	end = ptr;
	while (*end)
		++end;

	if (end > ptr) {
		for (--end; end >= ptr && (*end == ' ' || *end == '\t' || *end == '\r' || *end == '\n'); --end);
	}
	memmove(str, ptr, (size_t) (end - ptr + 1));
	if (((unsigned int) (end - ptr + 1)) < strlen(str))
		str[end - ptr + 1] = '\0';
}

/* Alexey Frunze
https://stackoverflow.com/questions/8512958/is-there-a-windows-variant-of-strsep */
static char *
str_sep(char **stringp, const char *delim)
{
	char* start = *stringp;
	char* p;

	p = (start != NULL) ? strpbrk(start, delim) : NULL;

	if (p == NULL) {
		*stringp = NULL;
	} else {
		*p = '\0';
		*stringp = p + 1;
	}
	return start;
}

/* From Costya Perepelitsa
 https://www.quora.com/How-do-you-write-a-C-program-to-split-a-string-by-a-delimiter */
static char **
strsplit(const char* str, const char* delim, size_t* numtokens) {
	char *s = strdup(str);
	size_t tokens_alloc = 1;
	size_t tokens_used = 0;
	char **tokens = (char**) calloc(tokens_alloc, sizeof(char*));
	char *token, *rest = s;
	while ((token = str_sep(&rest, delim)) != NULL) {
		if (tokens_used == tokens_alloc) {
			tokens_alloc *= 2;
			tokens = (char**) realloc(tokens, tokens_alloc * sizeof(char*));
		}
		tokens[tokens_used++] = strdup(token);
	}
	if (tokens_used == 0) {
		free(tokens);
		tokens = NULL;
	} else {
		tokens = (char**) realloc(tokens, tokens_used * sizeof(char*));
	}
	*numtokens = tokens_used;
	free(s);
	return tokens;
}

static void
codeCount(xmlNode *topNode)
{
	xmlNode *node = NULL;
	int term = 0;

	for (node = topNode->children; node; node = node->next) {
		if (node->type == XML_ELEMENT_NODE) {
			if (term == 1) {
				xmlChar *value = xmlNodeListGetString(doc,
					node->xmlChildrenNode, 1);
				unsigned int valueLength = strlen((char *) value) + 1;
				char *tmp = (char *) malloc(sizeof(char) *
					valueLength);
				char *tmp2;
				int i;

				(void) strncpy(tmp, (char *) value, valueLength);
				xmlFree(value);
				trim(tmp);
				tmp2 = tmp;
				for (i = 0; tmp[i]; (tmp[i] == '\n') ? i++ : *tmp++);
				free(tmp2);
				if (codeMaxCount < (unsigned int) i) {
					codeMaxCount = (unsigned int) i + 1;
				}
				if (debug && codeMaxCount != 4)
					(void) printf("codeCount: max %u\n",
						codeMaxCount);
			}
			term++;
		}
	}
}

static void
moveCount(xmlNode *topNode)
{
	xmlNode *node;
	unsigned int move = 0;

	for (node = topNode->children; node; node = node->next) {
		if (node->type == XML_ELEMENT_NODE) {
			codeCount(node);
			move++;
		}
	}
	if (moveMaxCount < move) {
		moveMaxCount = move;
	}
}

static void
lessonCount(xmlNode *topNode)
{
	xmlNode *node;
	unsigned int lesson = 0;

	for (node = topNode->children; node; node = node->next) {
		if (node->type == XML_ELEMENT_NODE) {
			moveCount(node);
			lesson++;
		}
	}
	if (lessonMaxCount < lesson) {
		lessonMaxCount = lesson;
	}
}

static void
chapterCount(xmlNode *topNode)
{
	xmlNode *node;
	unsigned int chapter = 0;

	for (node = topNode->children; node; node = node->next) {
		if (node->type == XML_ELEMENT_NODE) {
			lessonCount(node);
			chapter++;
		}
	}
	if (chapterMaxCount < chapter) {
		chapterMaxCount = chapter;
	}
}

static void
editionCount(xmlNode *topNode)
{
	xmlNode *node;
	unsigned int edition = 0;

	for (node = topNode->children; node; node = node->next) {
		if (node->type == XML_ELEMENT_NODE) {
			chapterCount(node);
			edition++;
		}
	}
	if (editionMaxCount < edition) {
		editionMaxCount = edition;
	}
}

static void
bookCount(xmlNode *topNode)
{
	xmlNode *node;
	unsigned int book = 0;

	for (node = topNode->children; node; node = node->next) {
		if (node->type == XML_ELEMENT_NODE) {
			editionCount(node);
			book++;
		}
	}
	if (bookMaxCount < book) {
		bookMaxCount = book;
	}
}

static void
readMove(xmlNode *topNode, struct moveType *moveAbacus)
{
	xmlNode *node;
	char *tmp;

	if (debug) {
		(void) printf("\t\t\t\t\t%s\n", topNode->name);
	}
	moveAbacus->code = NULL;
	moveAbacus->lineText = NULL;
	moveAbacus->lines = 0;
	node = topNode->children;
	for (node = topNode->children; node && moveAbacus->lineText == NULL; node = node->next) {
		if (node->type == XML_ELEMENT_NODE) {
			xmlChar *value = xmlNodeListGetString(doc,
				node->xmlChildrenNode, 1);
			unsigned int valueLength = strlen((char *) value) + 1;
			tmp = (char *) malloc(sizeof(char) *
				valueLength);
			(void) strncpy(tmp, (char *) value, valueLength);
			xmlFree(value);
			if (debug) {
				(void) printf("\t\t\t\t\t%s\n", node->name);
				(void) printf("value: %s\n", tmp);
			}
			if  (moveAbacus->code == NULL) {
				moveAbacus->code = tmp;
			} else {
				unsigned int i;
				size_t lines;
				char **text;

				trim(tmp);
				text = strsplit(tmp, "\n", &lines);
				if (lines > codeMaxCount) {
					codeMaxCount = lines;
				}
				if (debug && codeMaxCount != 4)
					(void) printf("readMove: max %u\n",
						codeMaxCount);
				moveAbacus->lineText = (char **) calloc(lines, sizeof(char *));
				for (i = 0; i < lines; i++) {
					unsigned int textLength;
					trim(text[i]);
					textLength = strlen(text[i]) + 1;
					moveAbacus->lineText[i] = (char *) malloc(sizeof(char) *
						textLength);
					(void) strncpy(moveAbacus->lineText[i], text[i], textLength);
					free(text[i]);
				}
				free(tmp);
				free(text);
				moveAbacus->lines = lines;
			}
		}
	}
	if (moveAbacus->lineText == NULL && moveAbacus->code != NULL) {
		free(moveAbacus->code);
		moveAbacus->code = NULL;
	}
}

static void
readLesson(xmlNode *topNode, struct lessonType *lessonAbacus)
{
	xmlNode *node;
	unsigned int move = 0;
	xmlChar *name = xmlGetProp(topNode, (const xmlChar *) "name");
	unsigned int nameLength = strlen((char *) name) + 1;

	if (debug) {
		(void) printf("\t\t\t\t%s\n", topNode->name);
		(void) printf("\t\t\t\t    name: %s\n", name);
	}
	lessonAbacus->name = (char *) malloc(sizeof(char) *
		nameLength);
	(void) strncpy(lessonAbacus->name, (char *) name, nameLength);
	xmlFree(name);
	if (moveMaxCount > 0) {
		lessonAbacus->move = (struct moveType *)
			calloc(moveMaxCount, sizeof(struct moveType));
	} else {
		lessonAbacus->move = NULL;
	}
	for (node = topNode->children; node && move < moveMaxCount; node = node->next) {
		if (node->type == XML_ELEMENT_NODE) {
			if (debug) {
				(void) printf("\t\t\t\t\t%u\n", move);
			}
			readMove(node, &lessonAbacus->move[move]);
			move++;
		}
	}
	lessonAbacus->moves = move;
}

static void
readChapter(xmlNode *topNode, struct chapterType *chapterAbacus)
{
	xmlNode *node;
	unsigned int lesson = 0;
	xmlChar *name = xmlGetProp(topNode, (const xmlChar *) "name");
	unsigned int nameLength = strlen((char *) name) + 1;

	if (debug) {
		(void) printf("\t\t\t%s\n", topNode->name);
		(void) printf("\t\t\t    name: %s\n", name);
	}
	chapterAbacus->name = (char *) malloc(sizeof(char) *
		nameLength);
	(void) strncpy(chapterAbacus->name, (char *) name, nameLength);
	xmlFree(name);
	if (lessonMaxCount > 0) {
		chapterAbacus->lesson = (struct lessonType *)
			calloc(lessonMaxCount, sizeof(struct lessonType));
	} else {
		chapterAbacus->lesson = NULL;
	}
	for (node = topNode->children; node && lesson < lessonMaxCount; node = node->next) {
		if (node->type == XML_ELEMENT_NODE) {
			if (debug) {
				(void) printf("\t\t\t\t%u\n", lesson);
			}
			readLesson(node, &chapterAbacus->lesson[lesson]);
			lesson++;
		}
	}
	chapterAbacus->lessons = lesson;
}

static void
readEdition(xmlNode *topNode, struct editionType *editionAbacus)
{
	xmlNode *node;
	unsigned int chapter = 0;
	xmlChar *version = xmlGetProp(topNode, (const xmlChar *) "version");
	unsigned int versionLength = strlen((char *) version) + 1;

	if (debug) {
		(void) printf("\t\t%s\n", topNode->name);
		(void) printf("\t\t    version: %s\n", version);
	}
	editionAbacus->version = (char *) malloc(sizeof(char) *
		versionLength);
	(void) strncpy(editionAbacus->version, (char *) version, versionLength);
	xmlFree(version);
	if (chapterMaxCount > 0) {
		editionAbacus->chapter = (struct chapterType *)
			calloc(chapterMaxCount, sizeof(struct chapterType));
	} else {
		editionAbacus->chapter = NULL;
	}
	for (node = topNode->children; node && chapter < chapterMaxCount; node = node->next) {
		if (node->type == XML_ELEMENT_NODE) {
			if (debug) {
				(void) printf("\t\t\t%u\n", chapter);
			}
			readChapter(node, &editionAbacus->chapter[chapter]);
			chapter++;
		}
	}
	editionAbacus->chapters = chapter;
}

static void
readBook(xmlNode *topNode, struct bookType *bookAbacus)
{
	xmlNode *node;
	unsigned int edition = 0;
	xmlChar *name = xmlGetProp(topNode, (const xmlChar *) "name");
	xmlChar *author = xmlGetProp(topNode, (const xmlChar *) "author");
	unsigned int nameLength = strlen((char *) name) + 1;
	unsigned int authorLength = strlen((char *) author) + 1;

	if (debug) {
		(void) printf("\t%s\n", topNode->name);
		(void) printf("\t    name: %s\n", name);
		(void) printf("\t    author: %s\n", author);
	}
	bookAbacus->name = (char *) malloc(sizeof(char) *
		nameLength);
	(void) strncpy(bookAbacus->name, (char *) name, nameLength);
	bookAbacus->author = (char *) malloc(sizeof(char) *
		authorLength);
	(void) strncpy(bookAbacus->author, (char *) author, authorLength);
	xmlFree(name);
	xmlFree(author);
	if (editionMaxCount > 0) {
		bookAbacus->edition = (struct editionType *)
			calloc(editionMaxCount, sizeof(struct editionType));
	} else {
		bookAbacus->edition = NULL;
	}
	for (node = topNode->children; node && edition < editionMaxCount; node = node->next) {
		if (node->type == XML_ELEMENT_NODE) {
			if (debug) {
				(void) printf("\t\t%u\n", edition);
			}
			readEdition(node, &bookAbacus->edition[edition]);
			edition++;
		}
	}
	bookAbacus->editions = edition;
}

static void
readAbacus(xmlNode *topNode, struct libraryType *libraryAbacus)
{
	xmlNode *node;
	unsigned int book = 0;

	if (bookMaxCount > 0) {
		libraryAbacus->book = (struct bookType *)
			calloc(bookMaxCount, sizeof(struct bookType));
	} else {
		libraryAbacus->book = NULL;
	}
	for (node = topNode->children; node && book < bookMaxCount; node = node->next) {
		if (node->type == XML_ELEMENT_NODE) {
			if (debug) {
				(void) printf("\t%u\n", book);
			}
			readBook(node, &libraryAbacus->book[book]);
			book++;
		}
	}
	libraryAbacus->books = book;
}

extern struct libraryType abacusLibrary;

static void
setLibrary(xmlDoc *document) {
	/* Get the root element node */
	xmlNode *topNode = xmlDocGetRootElement(document);
	xmlNode *node;

	for (node = topNode; node; node = node->next) {
		if (node->type == XML_ELEMENT_NODE) {
			bookCount(node);
		}
	}
	if (debug) {
		(void) printf("%u %u %u %u %u %u\n",
			codeMaxCount, moveMaxCount, lessonMaxCount,
			chapterMaxCount, editionMaxCount, bookMaxCount);
	}
	for (node = topNode; node; node = node->next) {
		if (node->type == XML_ELEMENT_NODE) {
			if (debug) {
				printf("%s\n", node->name);
			}
			readAbacus(node, &abacusLibrary);
		}
	}

	/* Free the document */
	xmlFreeDoc(document);

	/*
	 * Free the global variables that may
	 * have been allocated by the parser.
	 */
	xmlCleanupParser();
}

static xmlDoc *
getDoc(const char *fileName) {
	doc = xmlReadFile(fileName, NULL, 0);
	if (doc == NULL) {
		(void) fprintf(stderr, "Could not parse file %s\n", fileName);
	}
	return doc;
}

void
readParseFile(const char *fileName) {
	/* treating as a singleton */
	if (abacusLibrary.book != NULL)
		return;
	doc = getDoc(fileName);
	if (doc != NULL)
		setLibrary(doc);
}

/**
 * Simple example to parse a file called "file.xml",
 * walk down the DOM, and print the name of the
 * xml elements nodes.
 */
#if 0
char * xmlFile = XML_FILE;
int
emain(int argc, char **argv)
{
	if (argc >= 2) {
		xmlFile = argv[1];
	}

	/*
	 * This initialize the library and check potential ABI mismatches
	 * between the version it was compiled for and the actual shared
	 * library used.
	 */
	LIBXML_TEST_VERSION
	readParseFile(XML_FILE);
	return 0;
}
#endif
#else
#ifndef WINVER
static int displayedError = 0;
#endif

void
readParseFile(const char * fileName) {
#ifndef WINVER
	if (displayedError == 0) {
		(void) fprintf(stderr,
			"%s not read, XML tree support not compiled in.\n",
			fileName);
		displayedError = 1;
	}
#endif
}
#if 0
int emain(void)
{
	(void) fprintf(stderr, "XML tree support not compiled in.\n");
	exit(1);
}
#endif
#endif
