  [Feb 15, 2019]  V8.2.1:
    Fixed spinner to not wrap and be unlimited where appropriate.
  [Oct 02, 2018]  V8.2.0:
    Removing strcpy and strcat calls by migrating to strncpy and strncat.
      Added HAVE_SNPRINTF compile-time option.  Did for all files except
      AbacusT.c, (for teach mode) here it was reduced but not eliminated.
    Fixed drawing especially for Roman Hand Abacus and Medieval Counter.
    Fixed clearing decimal when changing format from Roman Hand Abacus.
    Fixed problem pointed out by clang using uninitialized variables.
  [Jul 21, 2018]  V8.1.8:
    Fixing sound installation.
    Fixing potential some buffer overruns pointed out in mingw in
      AbacusT.c.
  [Apr 03, 2018]  V8.1.7:
    Updated to use spinner (using latest Motif  ;) ).
  [Feb 05, 2018]  V8.1.6:
    Added rail shading.
  [Nov 16, 2017]  V8.1.5:
    Fix for railIndex, can now be set in generic mode.
    Russian abacus the spaces were increased by 3.
    Adding an experimental feature, gettext for locales.  Its only
      experimental an only used for teach mode.  For now Motif does not
      handle non-ASCII very well.  Looking to just handle a simple
      change first like French accents.  Any examples of handling this
      in Motif would be appreciated.
    Spelling of Suanpan, Jupan, and Schety corrected.
    Updated look of "X" and "<>" in medieval abacus.
  [Oct 15, 2017]  V8.1.4:
    Teach fix for (division or root) and piece rail.
    Teach fix for for lower bounds of rails with cube roots.
    Teach fix for presentation of groups for root operations.
    Removed gcc-7 warnings. 
  [Oct 01, 2017]  V8.1.3:
    Updates for 64 bit compilation in Windows.
    Quarter percent now independent of piece rail for quarter and
      twelfths. 
    Fixing when subdeck and changing format.  Also a could crash
      when changing format on some machines.
    Fix a long standing bug when adding under teach and piece rail
      is set.
    Windows now grays out options where appropriate.
  [Aug 21, 2017]  V8.1.2:
    When showing demo moves it now highlights the rail being moved.
    Added GUI interface for Ancient Roman in display and Modern Roman
      on rails (not historical), also will show fractions in Latin.
    Turn off Roman Numeral display when using decimal fractions.
    Subrails and quarters now mapped in Roman Numerals (by factor of
      3, not historical).  (Symbols on right are not correct but not
      correct for subdeck eighths either.)
    Adding/removing sign no longer clears subrail.
    Removing subdeck or format left extra values and beads.
    Fixed eighth sensitivity issue in Motif menu.
  [Aug 01, 2017]  V8.1.1:
    Valgrind helped find some definite memory leaks.
    Show last move in lesson.
  [Jul 17, 2017]  V8.1.0:
    Removed the lesson files and now use same abacusDemo.xml in Java port.
    Added fallback code to use a static demo if can not read XML demo file.
      Windows version always uses fallback, some issue with libxml2-2.dll
      at runtime.  The X version if compiled without libxml2 will also use
      fallback to use static demo.
    Added another book from Abacus Guide.  Only first few chapters and
      examples so far.  (Not available in fallback mode.)
    Updated sound configuration for X version.  Uses a simple included
      script play.sh, that is now used by default.
  [Sep 03, 2016]  V8.0.8:
    Updates to Roman display.
    Old website http://www.tux.org/~bagleyd died, shifted code base to new
      website http://www.sillycycle.com
  [Jan 18, 2016]  V8.0.7:
    Bases other than 10 broken since 8.0.0.
  [Dec 12, 2015]  V8.0.6:
    Fixed the "wrench" look for X when showing anomalies.
    Added group markers after last anomaly.
  [Oct 22, 2015]  V8.0.5:
    Added small pointer for feedback when moving decimal point.
  [Sep 28, 2015]  V8.0.4:
    Some lessons had some lines too long causing problem.
  [Feb 24, 2015]  V8.0.3:
    Sound and install fixes.
    Fix for switching from Chinese to Medieval and vice versa.
  [Nov 28, 2014]  V8.0.2:
    Many bugs fixed for the new Counter including highlighting.
  [Nov 18, 2014]  V8.0.1: Not released
    Medieval Counter added.  Click left of X to add or right of X to
      subtract.  Middle of line is a counts as 5 (for base 10).
  [Mar 18, 2014]  V8.0.0:
    Added drip sound for format change.
    Moving the decimal point needs a select and a release to a different
      point on "tape".  Now behavior more consistent with a real abacus.
      Also consistent with Android, as a big finger moving beads would
      too easily move decimal point.
    Scalable frame.
  [Jan 20, 2014]  V7.7.3: Not released
    Reorganized pixmaps directory.
  [Jan 10, 2013]  V7.7.1:
    Fixed draw of diamond beads when vertical.
    Added scrolls to help dialogs where needed.
    "xmabacus difficult to use": criticism was not clear but added:
      Submenus for controls.
      Moved sliders for base into menu.  Added delay slider in menu.
        Motif has problems with sliders in menus so they are actually
        in a pop-up.
      Added toggles to many toggles to menu buttons.
      Moved Learn stuff into menubar and base stuff into menu like
        Java version.
      Made "Generic" mode disabled in menu (only can use if set at start).
      Added check boxes to menus.  Subdeck and eighth enabling etc
        kind of complicated as it needs exactly 2 places for decimal,
        twelfth (or quarter), and slots (Roman). I hope I got all the cases.
      Radio buttons added for museum choice, disabled in all but Roman
        Hand-Abacus.
    "xmabacus not all fields labeled": Primary and Auxiliaries are now
      labeled.  It disappears when prompt is used.  Color of text is 2
      shades darker than the default bead.  Done only for Lee's abacus.
    Motif: quick "o" or "$" does not change gui choice
    Xt: pressing "o" twice causes "Warning: XtRemoveGrab asked to remove
      a widget not on the list".  Instead of XtDestroyWidget just
      XtUnrealizeWidget shell.  Add NULL check when creating shell.
    Windows + Roman Numerals: problem with doing "calculate".
    Windows + Anomaly/Watch: failing because of MAX_INT... needed to
      change from 2^63-1 to 2^31-1.
    "xmabacus hard to read": new bold fontlist in Abacus resource file
      should address this.
  [Aug 31, 2012]  V7.7.0:
    Added mouse scroll.  Also arrow keys with mouse work similarly now.
    Abacus control left/right (or if vertical up/down) changes number of
      rails.
  [Jan 28, 2011]  V7.6.8:
    Had to reorganize letters to be in sync with Java version.
      v Roman Nvmerals (was m)
      m Museum (was e)
      e Eighth (was h)
      (h now used in attach/detach in Java html window)
    Fixed a core dump when switching from eighths to twelfths.
  [Sep 29, 2010]  V7.6.7:
    An old version got out in 7.6.6.
  [Sep 27, 2010]  V7.6.6:
    Cbrt in teach now works, try like "3u" (cbrt of 3).
    It flashes now when reached final answer in teach mode.
    -decimalPosition now sets auxiliary rails also.
  [Sep 02, 2010]  V7.6.5:
    "2v" (sqrt of 2) in teach now works.  Place decimal place appropriately
      to give wanted precision.
  [Aug 03, 2010]  V7.6.4:
    Work around for LessTif bug 2801540.
      Problem is that LessTif will not automatically adjust its
      displayed widgets to accommodate a new widget after the topLevel
      is realized.  So when choice of Teach/Demo/Base is chosen the
      subwindow would have height of 1.  Added a force to adjust the 
      frame in xabacus.c, if initial size of new window less than 2.
      Works fine in Motif/OpenMotif.  Did not use a check LESSTIF_VERSION
      because it could be compiled on one and later used with different
      library.
  [Jul 06, 2010]  V7.6.3:
    Highlighting of beads added in teach mode.
  [Jul 01, 2010]  V7.6.2:
    Highlighting of rail added in teach mode.
  [Apr 14, 2010]  V7.6.1:
    Ran cppcheck which showed me I was using some nonportable sprintf's.
      Now updated to use strcat.
  [Jan 21, 2010]  V7.6:
    Version handling changed to a single version.h (and man page).
    Sound fixed for esound.
  [Nov 10, 2009]  V7.5.5:
    Windows version: fix to make demo, calc and teach dialogs consistent
    Windows version: added accelerator for references.
    teach division information: remove redundancy for single digit calc
    more teach information for add/subtract
  [Nov 02, 2009]  V7.5.4:
    Fractional fix for division.
    Teach mode base fixes: division, multiplication, lee.
    Teach mode: fixes for error handling.
    In progress: Demo of sqrt
  [Oct 15, 2009]  V7.5.3:
    Teach mode: bug fixes and better error handling.
    Lee's abacus: handle teach mode when formats are different.
    Resynced C and Java code, man pages should closely relate to both
      now.
    Started on sqrt and cbrt but have trouble understanding the method
      in "How to Learn Lee's Abacus" by Lee Kai-chen (which is using
      Newton's method).
    Started on colorizing "Teach" to make clear the current position
      of calculation for digit and rail.  Nothing to show for it yet,
      but new "highlight" colors will be used for this.
  [Oct 05, 2009]  V7.5.2:
    Division using teach should now work.
    Windows version has no auxiliary rails so this part just has to be
      imagined.  Values from current answer are just represented on
      this rail so this is not a big leap.
  [Jul 30, 2009]  V7.5.1:
    Found problem with borrow for subtraction and teach (e.g. 39-28).
    Fixed demo to work more in sync with Java version.
    Multiplication using teach should now should work.
    Added lee options for controlling color and number of rails for
      auxiliary decks from command line.
  [Jun 05, 2009]  V7.5:
    Cleaned up menu look.
    Fixed again for NAS sound.
    Added "Teach" utility to allow teaching of addition and subtraction.
      Tables are taken from "How to Learn Lee's Abacus" by Lee Kai-chen.
    Generalized so bases should also work with "Teach".
    Fixed bug in calculate when using quarters or quarter percents.
    Changed name of flexible mode from "Other" to "Generic" and changed
      default to be Chinese.
    Moving beads when base was large for Russian or Danish format could
      cause a false display of corruption error.
    Put non-English names of Abacus in pull-down.
    Added complement function, may be useful in future for negatives.
    Added rightToLeft for "Teach".  With it you can change from the
      traditional left to right for addition and subtraction.
  [Dec 14, 2008]  V7.4.3:
    Fixed for NAS sound.
  [Jul 21, 2008]  V7.4.1:
    Strange character at end of help title removed.
    Quit menu selection now works.
    Fix for make install-png, thanks to solsTiCe d'Hiver <solstice.dhiver
      AT gmail.com>.
  [Jul 11, 2008]  V7.4:
    Leading function names in lowercase.
  [Jun 01, 2008]  V7.3.9: Not released
    Made "t" and "b" names consistent with Java version ie, -tpiece now
      -topPiece and -bpiece now -bottomPiece.
    Japanese format 1,000,000,000,000,000,000,000,000.0+1 corruption fix.
      Still does not add accurately as the math is using double.
    Fixed various corruption and overflows.
  [May 18, 2008]  V7.3.8:
    Fixed goof in ancientRoman.
  [May 12, 2008]  V7.3.7:
    Allow latin for fractions when using Roman Numerals with -latin.
    Allow signs on bars to be "modern" Roman Numerals with -modernRoman.
    Allow older Roman Numerals to be displayed with -ancientRoman.
    No group positions on abacus when anomaly is on.
  [Apr 11, 2008]  V7.3.6:
    Allow "," for decimal point and "." for group separator.  Probably,
      should be done using locale but there are so many ways to configure
      the abacus, I was not sure I should make an exception here and
      configure it in a different way.
    Fixed bottom bead click which was 3 pixels off.
    -pressOffset option added.  Beads are now closer together.
      Use option to access older way.
      Beads are rounder with light bordering to distinguish beads easier.
    Added color options for auxiliary beads.
  [Apr 08, 2008]  V7.3.5:
    Eighths half added to menu and off by one error.
    Eighths 1/2 bead was showing wrong value.
    Commas should be ignored in calculations.
    Implemented calculations for anomalies.
    Calculation support up to base 20, which historically,
      was the maximum base used.  Babylonians used 60, but was a
      conglomeration of base 10 and 6.
  [Feb 29, 2008]  V7.3.4:
    Calculations like 4! can now be done in auxiliary fields.
    Auxiliary text windows now proper sizes.
    -script now defaults to stdout for X-Windows.  Aux stuff now works
      here also.
    Changed pointer from crosshair to hand2.
    gs has problems reading the distributed postscript file.  In pixmaps
      directory, I now distribute gif [now png] files in case there are others
      like me who have trouble reading it.  As of now, these are not installed
      automatically.
  [Dec 24, 2007]  V7.3.3:
    Constants changed.
  [Nov 06, 2007]  V7.3.2:
    Added -nolee option when compiled with -DABACUS_LEE.
    Merging in GNU standards.
    Abacus Demo upgrade to allow chapters.
    Variable xpm pixmaps controlled by resource file.
    Default "Other" is now not printed on non-Motif.
    Number in X version on left side in-case window is small.
    Updated action codes and synchronized Java and C code.
    Various small changes.
  [Jul 26, 2007]  V7.3.1:
    Updated action codes and synchronized Java and C code.
    Added eighths ability for Roman Hand Abacus, as suggested by Stephen
      Stephenson.  Setup for UK Museum version may not work as expected on
      FR and IT versions.
    Condense various menu defines to one set: "ACTION_*".
    Windows fix for strange memory corruption when 'o' pressed for demo mode.
    Windows now has black text for demo by default instead of green.
  [Feb 23, 2007]  V7.3:
    HTML no longer corrupted.
    mono and reverseVideo update.
    Named fractions added for Roman numerals using 12ths and subdeck.
  [Feb 20, 2007]  V7.2.8:
    Roman Hand Abacus fix for subdeck part 1/3 of 1/12 should really be
      1/12 of 1/12 thanks to Ray Greaves <rayg AT base12.plus.com>.
  [Feb 20, 2007]  V7.2.7:
    Setting up Roman Hand Abacus from .ad file caused core.
    Fix for setting up twelfths and quarters from .ini file.
    New bump.au sound, db lowered, and bumpdat.au removed, thanks to
      http://audacity.sourceforge.net to help clean the sound up.
    Setting decimal a little off for vertical.
  [Oct 11, 2006]  V7.2.6:
    Option to format output to use commas so numbers can be read easier,
      shortcut use "g" for group.
    Fixed an overflow if number entered in display is too large.
    Fixed menu to assure the OK part of queries is handled correctly.
    Now use http://netpbm.sourceforge.net to generate images from xpm.
  [Aug 08, 2006]  V7.2.5:
    Roman subdeck to Russian error fixed (ffftb.04f).
    Roman subdeck error fixed (ffftb.04b).
    Roman fix for pieces and piece percents.
    Russian to Danish fix for twelfths.
    Added more debugging.
    Fix for decreasing number of rails with special rails by slider.
  [Jul 21, 2006]  V7.2.4:
    Fixed assorted memory leaks and uninitialized memory access reads.
  [May 14, 2006]  V7.2.3:
    Tan frame.
    Danish Elementary School abacus added (teaching aid).
      Do not know how its used, setup as:
        clears when pushed left
        lowest order beads at bottom
        kept simple so decimal point is at bottom
  [Mar 06, 2006]  V7.2.2:
    Fixed bug for signs and quarter percents and subdecks.
    Changed definition of CARRY buffer from 2 to 1, fixed display bug.
    Anomaly now works when carries are possible.  Although there is no
      historical abacus like this, its nice to remove this requirement.
    Pieces and piece percents now use "+" on bar except Roman Abacus.
    Right click clear fix (off by one).
    Made usage string compatible with versions of Motif and screen.
    Fixed a problem for quarter percents where beads would get stuck.
    Windows fix for printing of subdeck chars in Roman Abacus for Museum
      of the Thermae in Rome.
  [Feb 27, 2006]  V7.2.1:
    rngs.h left out of distribution, also some permission problems
      on installation scripts.
    Simplified code to switch to and from Russian abacus.
    Redesigned rounded beads to always account for slots.  I.e. a bead is
      either an overlay of 2 circles and a rectangle if a wide bead or an
      overlay of 4 circles and two rectangles (a cross) where there would
      always be a flat edge along the width of a rail or slot.
  [Jan 17, 2006]  V7.2:
    Added on 3 subdecks with 4 beads.
      ./xabacus -roman -rails 10 -tpiece 2 -bpiece 6 -subdeck 3 -subbead 4
      1/2,1/4,1/3 ounces column now work for a Roman Abacus.  This
      comes in two flavors, where possibly the simpler one, needs 3 decks
      (where a single bottom column would be) (one known example in
      museum in British Museum in London).  In the other flavor, the
      beads could stop midway on a single slot (in IMHO not a well defined
      way) (two known examples one in Museum of the Thermae in Rome and
      one in Cabinet des medailles, Bibliotheque nationale, Paris).
      Type "e" or use Museum option to cycle through museum pieces.
    Added older Roman Signs "|x| (((|))) ((|)) (|) c x |" on bar.
      Also allow these signs to be Roman Numerals.
    If anomaly, calculations are not allowed.
    Switching from/to Russian with a negative was broken.
    Added more sanity checks for configuration, or it could go into a
      weird state.
    Fixed for small beads and PC version, would not have rounded slots.
    Fixed top level room and spaces if top pieces or piecePercents
      greater than 2.
  [Jan 05, 2006]  V7.1.8:
    There were some cases where resizing the number of rails causes
      the calculation to change unnecessarily.
    Signed overflow fixes e.g. 50000000000*2=-5000 or
      -100000000000 Chinese converted to Japanese.
    Assorted tweaks discovered when porting code to Java version.
    Moved Anomaly from "a" to "l" to be consistent with Java version.
    Move bar problems with quarter percent.
    Quarter percent for Russian format would give an X error, fixed.
  [Jan 01, 2006]  V7.1.7:
    Sign, quarter, and quarterPercent code all rewritten to be less buggy.
    Quarter percents option for 1/4 Kopecks.
    Use of "piece" instead of "quarter" for more variability.  This now
      allows a "twelfths" capability (Roman ounces) for Roman Hand Abacus.
      Its pretty configurable (with the configuration file) so you can have
      a range from halves to sixteenths (stock market pricing, pieces of
      eight, etc), and possibly beyond if you want to get crazy.
    New symbols on the bar for "-" for sign, "O" for pieces.  If the base and
      piece and format in Russian style set weird it could otherwise be hard
      hard to read.  O used because it was used on the Roman Hand Abacus.
    Anomaly option for Mesoamerican Nepohualtzintzin Abacus.  Basically a
      JP Abacus in base 20 (3/4) where the columns left of the 2nd digit are
      multiples of 360 (instead of 400).  4th column of unit beads would be
      20x360.  Bar marked with an "X" where the anomaly is.  Note: this
      is turned off if there are extra beads as in the Chinese mode or
      Korean mode or this leads to even more unexpected anomalies.
    AnomalySq option to make more Anomaly more configurable, now can have a
      representation of time using hours, minutes, seconds.  Babylonians are
      responsible for that mess.  Though they probably did not use an
      abacus.
  [Nov 19, 2005]  V7.1.6:
    Vertical for proper Russian schoty (rotate xabacus clockwise 90 degrees).
      Use "v" or from the command line -vertical.
    The first bead that is 3 decimal places >= 1000 gets a darker color
      on Russian Abacus.
  [Oct 05, 2005]  V7.1.5:
    -version added
    Common option help for X and X-Motif
  [May 31, 2005]  V7.1.4:
    Separated out compatible components.
  [Nov 21, 2004]  V7.1.3:
    Bug fixes for auto-calculation and small base values.
    Thanks to Debian maintainer Florian Ernst <florian AT uni-hd.de> for
      esound fix.
  [Oct 22, 2004]  V7.1.2:
    Small changes synchronizing with Java code.
    Fixed goof introduced in previous which make quarter not work right.
  [Sep 25, 2004]  V7.1.1:
    Installation of sound files.
    bufferBead and colormap not properly initialized and could cause core
      dumps.
  [Aug 23, 2004]  V7.1:
    Thanks to Debian maintainer Florian Ernst <florian AT uni-hd.de> for
      pointing out that the demo breaks without Motif.  Fixed this and 'f'
      should now work better for X.
    Beads are now double buffered.  Faster and looks better on slow machines.
    Beads now slide in small increments.
    Got rid of turning the beads "inside-out" when pressed.  Too annoying.
    "<", ">" to speed up and slow down movement of the beads.  Actually,
      it changes "delay" inversely.
    Got rid of some drawing errors when beads are long.
    Sound added.
    Sort of real time using getttimeofday.
  [May 31, 2004]  V7.0.6:
    Demo now works with the other "standard" abaci.  Korean (or Pre-WWII
      Japanese) 1/5 (ko), Japanese 1/4 (or Roman) (jp), or Russian 0/10
      (ru).
  [Jan 23, 2004]  V7.0.5: Not released
    The idea with the calculator stuff is to see the beads
      move with input from the display line.  This will do a calculation
      but it is not meant as a demonstration of a mathematical operation.
      Do not use this for your taxes yet...
      Has some special features... 2v = sqrt of 2, 2u = cbrt of 2, e for e,
        p for pi, ! for factorial (should do a gamma function if real?), and
        ^ for power.  Display Base must equal Abacus Base.  Eventually want
        to do operations on strings so abacus could be an "infinite" size.
      Answers that result in an error default to zero (maybe all the beads
        should fall off?).
  [Jan 13, 2004]  V7.0.4:
    Clear for Lee's Abacus is for each individual deck except for demo.
    Have the decimal point setting accessible to the demo.
      I treat the decimal point as the place setting vernier...
    Resources broken in 7.0.2 fixed.
    Decrementing with the decimal on the right fixed.
    Added the faster diamond shaped beads.  :)
    Color of decimal point the same as beads.
    Put the decimal point (or place setting vernier) at a post.  (To me it
      should be between posts but have not seen an historic example).
    Windows fixes: 'u' for quarters mode and rounder beads
  [Dec 31, 2003]  V7.0.3:
    The Roman mode forces a plate look where the beads move in slots.
    Fixed minor drawing errors in Windows.
    Changed order of additions in the 4th lesson to conform with the
      standard way of doing multiplication.
    Fixed bug where a new 5th lesson not installed.
    Fixed clean on auxiliary decks from demo.
    Fixed demo mode on auxiliary decks.
  [Dec 23, 2003]  V7.0.2:
    Lee's Abacus, default setup when using Motif.  Added a 4th lesson
      for multiplication.  Find out more about Lee's Abacus from
      http://www.ee.ryerson.ca/~elf/abacus/leeabacus/
    Type of abacus listed for non-Motif and Windows.
    Demo can now be set dynamically in Windows.
  [Dec 15, 2003]  V7.0.1:
    Increment when decimal point left most bug fix.
    The Hide functionality of the Escape key now iconifies or drops in
      task bar.
    Added help in Windows.
    Accelerator Keys (F keys and regular keys) in Windows now work.
  [Nov 08, 2003]  V7.0:
    Abacus??.ad* files removed, info is in Abacus.ad*.
    Integrated winpuz6.5 (Windows 3.1/95) into X source tree.
  [Aug 28, 2003]  V5.7.3:
    png for Gnome and KDE menus and xpm for CDE menus.
    Added xpm for icon.
    Reset a few colors for consistency.
  [Aug 14, 2003]  V5.7.2:
    man 1 to man 6 and DESTDIR added and suggestions for ad.in stuff
      thanks to Stanislav Brabec <sbrabec AT suse.cz>
    The demo should work if the demo files are in the installation directory
      (the default but can be changed by -demopath) or the current
      directory.
    Bug fix for xabacus -japanese -demo and a few involving "quarters".
    User interface now free of numbers for formats.
  [Aug 11, 2003]  V5.7.1:
    When changing base certain combinations would create too many beads
      in top deck, beads are instead set to zero.
    Reformatted with option menus.
    Help kicked over.
    Added Korean and Roman.  Roman is essentially the same as the Japanese
      version right now.
  [Jul 07, 2003]  V5.7:
    Sync up with xpuzzles
  [Apr 29, 2003]  V5.6.2: Not released
    Added callback for calculate... does not really do too much yet.
      Need to #define CALC to try...
    Bug fix when in Demo and Russian format is pressed
    Clear now checks if you really want to clear using left mouse button,
      'c' will not query
    'Esc' will hide abacus (actually osfCancel did the trick)
    Help & About expanded and pop-ups added
    Took out bitmaps since it takes too much room
    Demo can be activated from within window without needing command-line
      -demo option.  Removed leak in loadFont.  Added 'o' functionality.
  [Mar 21, 2003]  V5.6.1:
    Added runtime options -chinese, -japanese, and -russian
    Fixed shading if beads are black
    Installation fixes for Motif and X versions
    Help & About expanded and pop-ups added
    Roman Numerals added
    Fixed uninstall on Cygwin
    Took out toggles and bitmaps since it takes too much room.
      Now compile-time option.  There is real help and one can change
      the toggles through the menu.
  [Dec 16, 2002]  V5.6:
    configure if around 2nd AC_CHECK_FUNC
    removed Sun CC (char *) warnings
    removed hard coding of program name
    Cygwin changes ./, -lSM -lICE
    x and xm files combined using HAVE_MOTIF
    port back from Java (on the Java side I got some help from
        Sarat Chandran <saratcmahadevan AT yahoo.com>)
      colors changes
      frame color change on enter/leave
      background/foreground set by default
      3d beads
      reset maximum
      pixel width
      "," diamonds
      added signs and quarters (with smaller delay when moved)
      Russian abacus (schoty) (it fell over)
  [Sep 01, 2001]  V5.5.4: g++ 3.0 warnings removed.  Simplified format.
    Added demo error checking.
  [Aug 11, 1999]  V5.5.2: Fixed runtime library problems when using configure,
    added text to README and xabacus.man, fixed confusing demo start.
  [Jul 31, 1999]  V5.5.1: Removed HP usleep warning.
  [Jun 20, 1999]  V5.5: Combined the independent work of Luis Fernandes'
    xabacus 1.00, also started in 1991.   This includes all the features
    to teach the users how to use the abacus (-demo) and a more solid
    frame (its sturdier :) ).  -bars changed to -rails to conform with
    the terminology.  Abacus now defaults to the top beads up.  The
    picture on the cover of my abacus instruction booklet was deceiving.
    Abacus.ps and Abacus?.les are written entirely by Luis Fernandes
    <elf AT ee.ryerson.ca>.  (Also see http://www.ee.ryerson.ca/~elf/abacus).
    Added -rv and -mono options.
    Implemented -base option, can now go up to base 36, yeah I got carried
    away here (it should even work with EBCDIC though untested (really
    carried away)).
    Dynamic number of rails, with calculations saved with each change.
  [Jan 01, 1997]  V5.4: configure, man page updates.
  [Apr 22, 1996]  V5.3: You can now see beads move one unit space at a time.
    This is governed by a new resource, "delay".
  [Feb 12, 1996]  V5.2.1: beads now invert when pressed, wprintf name may
    conflict with other UNIX libraries - changed to
    motif_printf, keyboard may not work with motif - fixed.
  [Jan 31, 1996]  V5.2: Dynamic number of rails (well, the widget is dynamic
    but xabacus.c limits it to around 300 for the title text),
    fixed another bug when resized (negative radius circles).
  [Dec 15, 1995]  V5.1: Fixed a bug when resized.
  [Sep 30, 1995]  V5.0: Xt/Motif, your choice.
  [May 16, 1995]  V4.10: Warnings removed from Sun's cc and lint.
  [Mar 13, 1995]  V4.3: Removed lint warnings and added a VMS make.com .
  [Nov 08, 1994]  V4.2: Removed gcc -Wall warnings.
  [Jun 03, 1994]  V4.1: R6.
  [May 07, 1994]  V4.0: Xt version.
  [Feb 03, 1993]  V3.0: Motif version.
    I got some good ideas from Douglas A. Young's book:
    "The X Window System Programming and Applications with
    Xt OSF/Motif Edition", particularly his dial widget.
    I got some good ideas on presentation from Q. Zhao's
    tetris.
  [Dec 17, 1991]  V2.0: XView version.
  [Feb 14, 1991]  V1.0: SunView version.
